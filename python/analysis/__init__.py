import os, sys, glob

import warnings
warnings.filterwarnings( action='ignore', category=RuntimeWarning, message='creating converter.*' )

if os.getenv('ANAROOT') is None :
    print "Attention, you did not setup. Run 'source setup.sh' before doing anything"
    sys.exit()

import ROOT
import cuts
import utils
from locations import loc, dataids
from outfiles import db, dumpDB, initDB, outfiles
initDB()

repo = os.getenv('ANAROOT')
ROOT.gROOT.ProcessLine('.x '+repo+'/pyutils/LHCb/lhcbStyle.C')



