configfile : 'cfg.yml'
print('CONFIGURATION:',config)

if config['test'] == 'True' : config['test'] = " --test "
else : config['test'] = ""

shell.executable("/bin/bash")
if config['cvmfs'] == 'True' :
    shell.prefix('cd $ANAROOT && source scripts/setup.sh && ')
else :
    shell.prefix('cd $ANAROOT && source scripts/setup.sh venv && ')


rule all:
     input : ['out/logs/log','out/tables/table_example.txt']
#    input: [ list of final outputs and optionally intermediate ones ]

#include : 'snake/fit_steps.snake' ## Just an example

rule print:
    input: ['out/logs/log']
    shell: "echo YES!"
    
rule test :
    input  : ['python/scripts/test.py']
    output : ['out/logs/log']
    shell : "python {input} && cat {output} "

rule test_template :
    input  : ['python/scripts/template_filling_example.py']
    output : ['out/tables/table_example.txt']
    shell : "python {input} {output} && cat {output}"

rule test_lbrun :
    #input : ""
    #output : ""
    shell : "lb-run DaVinci/latest gaudirun.py test.py"


