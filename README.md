## What is this repository.

This is a repository is a template for a generic LHCb anaylsis.
It includes support for ROOT and RooFit, EOS access, snakemake, Docker, gitlab CI runner

_Author: L. Pescatore_

Contact: luca.pescatore@cern.ch

## How to setup the repository

**Please do not clone the original but fork it first!** (which means make a copy of it under your user).
To do it just push the fork button from the main repository page.

Then clone the repository. N.B.: "--recursive" is important because it also clones submodules.

```git clone --recursive ssh://git@gitlab.cern.ch:7999/YOUR USERNAME/LHCb-Analysis-Template.git```

If you want to run interactively in testing mode.

```source scripts/setup.sh```

If you want to use snakemake.

```source scripts/setup.sh snake ```

If no cvmfs is available

```source scripts/setup.sh venv ```

in the latter case you'll have to take care that ROOT and all needed libraries are installed. For example this is done in the Docker image which can be created for this repository (see Docker folder).

#### First installation

To work properly you have to setup 2 virtual environments: venv and snake.

- snake: N.B.: This needs to be done **before** sourcing scripts/setup.sh.

``` source scripts/setup_snake.sh```

- venv : Should be automatically installed the first time you run the setup.

If CVMFS :

```source scripts/setup.sh ```

Otherwise:

```source scripts/setup.sh venv ```

### Environment

* The `ANAROOT` variable is now avilable pointing to the top folder.
* ROOT, matplotlib, sklearn, etc are setup.
* Put your code into the `python` folder should be automatically picked up if you try to import.
* At the beginning of each python file you should add `import analysis as an` (see later)


## Repository Structure

* The analysis code after stripping should be fully contained into the `scripts` and `python` folders.
If you make subfolders of `python` put an empty `__init__.py` file inside each one.
* `LHCb` contains generic LHCb stuff like the LHCb style file and the particle propeties.
* `output`: this folder should contain the lightweight outputs of your analysis
    1. `output/tables` is where you should put the output tables. 
    2. `output/plots` is where you should put output plots
    3. `output/logs` is where you should put output plots
* `templates` folder which should contain latex templates to be filled automatically.


### The analysis package (important!!)

This module loads the python environment: `import analysis as an`.

What will this do for you:

* Checking that you sourced the setup.sh file.

* Loading the LHCb style for plots.

* Make the cuts defined available to all python scripts. __All cuts should be defined into analysis/cuts!!__

* Make the locations easily available to you though the `loc` object. Already defined locations the following, feel free to define more as you need them:

    - loc.ROOT   = $ANAROOT
    - loc.PYTHON = $ANAROOT/python/
    - loc.LHCB   = $ANAROOT/LHCb/
    - loc.PLOTS  = $ANAROOT/output/plots/
    - loc.TABS   = $ANAROOT/output/tables/
    - loc.TMPS   = $ANAROOT/templates/

* Provide a common database saved on disk (still needs handling of more people working at same time).

```
import analysis as an
print an.db
{'Test':True, ...}
an.db['myeff'] = 0.99 ## Add a value
an.dumpDB() ## Saves it to disk
```

* Provide easy handling of output files

```import analysis as an 
an.outfiles.create("yields") ## Will create (only first time) the $ANAROOT/output/tables/yields.txt file and remember that it exists 
outfile.writeline("yields","N_B0 = 4000") ## Will just write a line into this file```

Or even better you can do the same using templates!!! Crate a file in the templates folder. You can write whatever you want into it just put the values to substitute into curly brackets
   
E.g.: `seleff = ${sel_eff} \pm {sel_eff_err}$`
   
And then use the `db` object to fill it!!

```import anaylsis as an
an.outfile.fill_template("efficiencies","eff_tmp",an.db)```
 
This will look for the keys into the db, fill them into your template and same everything to $ANAROOT/outputs/tables/efficiency.txt



### Access data

Raw data should be put on EOS. A function is provided to ls EOS from anywhere XRootD is installed:

```from utils import remotels as rem 
files = rem.remotels(SOME LOCATION ON EOS)```

It is highly recommended to save the locations of your datasets into a dictionary with labels
for easy retrieval. An example of this can be seen in python/analyisis/locations.py.

To see the available datasets: 

```import analysis as an
print an.dataids.keys()
['CL16', 'CL15', 'CL12', 'CL11']```


## Snakemake

You can run the offline anaylsis (or parts of it) simply typing `snakemake`.
Snakemake official tutorial: https://snakemake.bitbucket.io/snakemake-tutorial.html (start from Basics Step 1)
Snamekame tutorial by a LHCb user: 

The steps are defined into `Snakefile` in the top folder. At its beginning you can see a list of the required (final and intermediate) outputs of the analyisis.

To run snakemake from a clean shell:

```
source setup.ch snake
snakemake
```

## Docker

Docker support is available for this repository to allow running the analysis on any machine anywhere in the world (with internet). Please have a look at the readme inside the Docker folder.

## Common utilities

The `pyutils` folder contains utilities which you may find useful. See pyutils/README.md.


## Using SWAN
To be able to use SWAN, a few dedicated steps are necessary.
1. on lxplus, go to your CERNBOX user folder: /eos/user/.../.../
2. clone this repository (NB: the following procedure currently works only if you clone the repository from this location)
3. open a new SWAN session from https://swan.cern.ch , choosing LCG87, gcc49 and no environment script
4. open a new terminal, using the dropdown menu on the top right
5. cd into the repository's folder
6. run ```source setup-swan.sh install-deps```. This script is going to compile the tools, install rep and other dependencies. It is going to take a fairly long time for it to run, but it's needed only the very first time.
7. close the SWAN session
8. open a new one with the same caracteristics as before, but this time specify ```$CERNBOX_HOME/Lb2LemuAna/setup-swan.sh```
as environment script
9. enjoy

The instructions up to 7. included will need to be run only the first time, as a setup, while 8. and 9. are necessary every time you want to start a new SWAN session. 
