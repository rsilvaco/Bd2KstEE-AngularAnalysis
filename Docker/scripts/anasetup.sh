source /root/run_kinit.sh

if [ "$1" = "snake" ]; 
    then
        echo "Setup with snakemake"
        source activate snake
        export PYTHONPATH=$LB2LEMUROOT/python:$LB2LEMUROOT/pyutils:$PYTHONPATH
    else
        source $LB2LEMUROOT/setup_env.sh
fi
