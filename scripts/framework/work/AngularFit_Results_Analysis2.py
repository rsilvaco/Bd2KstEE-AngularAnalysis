# Document for results analysis (PDF outputs and loglikelihood values)
import random
import array
import math
import pickle
import argparse
import datetime
import time
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np

import sys, os
sys.path.append("../")
#below line for GPU usage
#os.environ["CUDA_VISIBLE_DEVICES"] = ""

import TensorFlowAnalysis as tfa
from tensorflow.python.client import timeline

from ROOT import TFile

import AngularFit_Inputs as AFI
import AngularFit_Models as AFM 
import AngularFit_Foldings as AFF

phsp = tfa.FourBodyAngularPhaseSpace()

# FSs, FSlist, FSnorms=nllcheck(params_list[8], 0.0,1.0,100, 0)
def nllcheck(param_num, minval, maxval, num, ensemble_num):
	
	# Variables that should be CHECKED! -------------------------------------------------------------------------------------------------------------
	params_list=AFI.GetTheoryParamsList("2.5_4.0", "FullAngDist_S", "False")
	model=AFM.FullAngDist_S
	use_file="bin2_q2_2.5_4.0"
	#----------------------------------------------------------------------------------------------------------------------------------------------------------
	
	parameter=params_list[param_num]
	
	data_ph = phsp.data_placeholder
	norm_ph = phsp.norm_placeholder

	Num_Threads=1

	init = tf.global_variables_initializer()
	sess = tf.Session(config=tf.ConfigProto( intra_op_parallelism_threads=1, inter_op_parallelism_threads=Num_Threads)) 
	sess.run(init)

	norm_sample = sess.run( phsp.UniformSample(1000000) )

	picin=open(use_file, "rb")
	ensemble_list=pickle.load(picin)
	
	original_val=parameter.init_value # Info. for resetting
	
	nll_vals_list=[]
	norm_list=[]
	params=np.linspace(minval, maxval, num)
	
	norm = tfa.Integral( model(norm_ph, params_list) )
	nll = tfa.UnbinnedNLL( model(data_ph, params_list), norm)
	
	for param in params:
		parameter.init_value=param
		parameter.update(sess, param)
		n=sess.run(nll, {data_ph : ensemble_list[ensemble_num], norm_ph : norm_sample}) # Using ensemble 0 out of a list
		a_norm=sess.run(norm, {norm_ph: norm_sample})
		nll_vals_list.append(n)
		norm_list.append(a_norm)
	
	# Restore original values
	parameter.init_value=original_val
	parameter.update(sess, original_val)
		
	return params, nll_vals_list, norm_list 

def PDFcheck(toy_mc, angles):
	cosThetaK_list=[]
	cosThetaL_list=[]
	phi_list=[]
	ThetaK_list=[]
	ThetaL_list=[]
	for i in range(0, len(toy_mc)):
		cosThetaK_list.append(toy_mc[i][0])
		ThetaK_list.append(math.acos(toy_mc[i][0]))
		cosThetaL_list.append(toy_mc[i][1])
		ThetaL_list.append(math.acos(toy_mc[i][1]))
		phi_list.append(toy_mc[i][2])
	if angles=="True":
		return ThetaK_list, ThetaL_list, phi_list
	if angles=="False":
		return cosThetaK_list, cosThetaL_list, phi_list
