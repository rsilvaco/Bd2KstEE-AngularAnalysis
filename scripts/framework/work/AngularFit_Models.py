# This document contains PDF models for fitting
import math
import tensorflow as tf

import sys, os
sys.path.append("../")
#below line for GPU usage
#os.environ["CUDA_VISIBLE_DEVICES"] = ""

import TensorFlowAnalysis as tfa

phsp = tfa.FourBodyAngularPhaseSpace()

# Full angular distribution PDF in P-basis with S-wave (Ref: LHCb-ANA-2013-097 p. 33)
# [cosThetaK, cosThetaL, phi]
def FullAngDist_P_Swave(x, params_list):
	FL, P1, P2, P3, P4p, P5p, P6p, P8p, FS, SS1, SS2, SS3, SS4, SS5=params_list
	
	cosThetaK = phsp.CosTheta1(x)
	cosThetaL = phsp.CosTheta2(x)
	phi = phsp.Phi(x)
	
	sinThetaK = tfa.Sqrt( 1.0 - cosThetaK * cosThetaK )
	sinThetaL = tfa.Sqrt( 1.0 - cosThetaL * cosThetaL )
	
	sinPhi=tfa.Sin(phi)
	cosPhi=tfa.Cos(phi)
	sin2Phi=tfa.Sin(2.0 * phi)
	cos2Phi=tfa.Cos(2.0 * phi)
	
	#sin^2(thetaK)
	sinTheta2K =  (1.0 - cosThetaK * cosThetaK)
	sinTheta2L =  (1.0 - cosThetaL * cosThetaL)
	cosTheta2L = cosThetaL*cosThetaL
	cosTheta2K = cosThetaK*cosThetaK
	
	#sin(2*thetaK)
	sin2ThetaK = (2.0 * sinThetaK * cosThetaK)
	sin2ThetaL = (2.0 * sinThetaL * cosThetaL)
	cos2ThetaL = (2.0 * cosThetaL * cosThetaL - 1.0)
	cos2ThetaK = (2.0 * cosThetaK * cosThetaK - 1.0)
	
	pdf = (3.0/4.0)*(1.0-FL)*sinTheta2K
	pdf += FL*cosTheta2K
	pdf += (1.0/4.0)*(1.0-FL)*sinTheta2K*cos2ThetaL
	pdf += (-1.0)*FL*cosTheta2K*cos2ThetaL
	pdf += (1.0/2.0)*(1.0-FL)*P1*sinTheta2K*sinTheta2L*cos2Phi 
	pdf += (tfa.Sqrt((1-FL)*FL))*P4p*sin2ThetaK*sin2ThetaL*cosPhi
	pdf += (tfa.Sqrt((1-FL)*FL))*P5p*sin2ThetaK*sinThetaL*cosPhi
	pdf += 2.0*(1.0-FL)*P2*sinTheta2K*cosThetaL 
	pdf += (tfa.Sqrt((1-FL)*FL))*P6p*sin2ThetaK*sinThetaL*sinPhi
	pdf += (tfa.Sqrt((1-FL)*FL))*P8p*sin2ThetaK*sin2ThetaL*sinPhi
	pdf += -(1.0-FL)*P3*sinTheta2K*sinTheta2L*sin2Phi 
	pdf *= (9.0/(32.0*math.pi))
	pdf *= (1.0-FS)
	
	pdf2 = FS*sinTheta2L
	pdf2 += SS1*sinTheta2L*cosThetaK
	pdf2 += SS2*sin2ThetaL*sinThetaK*cosPhi
	pdf2 += SS3*sinThetaL*sinThetaK*cosPhi
	pdf2 += SS4*sinThetaL*sinThetaK*sinPhi
	pdf2 += SS5*sin2ThetaL*sinThetaK*sinPhi
	pdf2 *= (3.0/(16.0*math.pi))
	
	pdf += pdf2
	
	return pdf

def FullAngDist_P4pfold_P_Swave(x, params_list):
	FL, P1, P4p, FS, SS1, SS2 =params_list
	
	cosThetaK = phsp.CosTheta1(x)
	cosThetaL = phsp.CosTheta2(x)
	phi = phsp.Phi(x)
	
	sinThetaK = tfa.Sqrt( 1.0 - cosThetaK * cosThetaK )
	sinThetaL = tfa.Sqrt( 1.0 - cosThetaL * cosThetaL )
	
	sinPhi=tfa.Sin(phi)
	cosPhi=tfa.Cos(phi)
	sin2Phi=tfa.Sin(2.0 * phi)
	cos2Phi=tfa.Cos(2.0 * phi)
	
	#sin^2(thetaK)
	sinTheta2K =  (1.0 - cosThetaK * cosThetaK)
	sinTheta2L =  (1.0 - cosThetaL * cosThetaL)
	cosTheta2L = cosThetaL*cosThetaL
	cosTheta2K = cosThetaK*cosThetaK
	
	#sin(2*thetaK)
	sin2ThetaK = (2.0 * sinThetaK * cosThetaK)
	sin2ThetaL = (2.0 * sinThetaL * cosThetaL)
	cos2ThetaL = (2.0 * cosThetaL * cosThetaL - 1.0)
	cos2ThetaK = (2.0 * cosThetaK * cosThetaK - 1.0)
	
	pdf = (3.0/4.0)*(1.0-FL)*sinTheta2K
	pdf += FL*cosTheta2K
	pdf += (1.0/4.0)*(1.0-FL)*sinTheta2K*cos2ThetaL
	pdf += (-1.0)*FL*cosTheta2K*cos2ThetaL
	pdf += (1.0/2.0)*(1.0-FL)*P1*sinTheta2K*sinTheta2L*cos2Phi 
	pdf += (tfa.Sqrt((1-FL)*FL))*P4p*sin2ThetaK*sin2ThetaL*cosPhi
	pdf *= (9.0/(32.0*math.pi))
	pdf *= (1.0-FS)
	
	pdf2 = FS*sinTheta2L
	pdf2 += SS1*sinTheta2L*cosThetaK
	pdf2 += SS2*sin2ThetaL*sinThetaK*cosPhi
	pdf2 *= (3.0/(16.0*math.pi))
	
	pdf += pdf2
	
	return pdf
	
def FullAngDist_P5pfold_P_Swave(x, params_list): 
	FL, P1, P5p, FS, SS1, SS3=params_list
	
	cosThetaK = phsp.CosTheta1(x)
	cosThetaL = phsp.CosTheta2(x)
	phi = phsp.Phi(x)
	
	sinThetaK = tfa.Sqrt( 1.0 - cosThetaK * cosThetaK )
	sinThetaL = tfa.Sqrt( 1.0 - cosThetaL * cosThetaL )
	
	sinPhi=tfa.Sin(phi)
	cosPhi=tfa.Cos(phi)
	sin2Phi=tfa.Sin(2.0 * phi)
	cos2Phi=tfa.Cos(2.0 * phi)
	
	#sin^2(thetaK)
	sinTheta2K =  (1.0 - cosThetaK * cosThetaK)
	sinTheta2L =  (1.0 - cosThetaL * cosThetaL)
	cosTheta2L = cosThetaL*cosThetaL
	cosTheta2K = cosThetaK*cosThetaK
	
	#sin(2*thetaK)
	sin2ThetaK = (2.0 * sinThetaK * cosThetaK)
	sin2ThetaL = (2.0 * sinThetaL * cosThetaL)
	cos2ThetaL = (2.0 * cosThetaL * cosThetaL - 1.0)
	cos2ThetaK = (2.0 * cosThetaK * cosThetaK - 1.0)
	
	pdf = (3.0/4.0)*(1.0-FL)*sinTheta2K
	pdf += FL*cosTheta2K
	pdf += (1.0/4.0)*(1.0-FL)*sinTheta2K*cos2ThetaL
	pdf += (-1.0)*FL*cosTheta2K*cos2ThetaL
	pdf += (1.0/2.0)*(1.0-FL)*P1*sinTheta2K*sinTheta2L*cos2Phi 
	pdf += (tfa.Sqrt((1-FL)*FL))*P5p*sin2ThetaK*sinThetaL*cosPhi
	pdf *= (9.0/(32.0*math.pi))
	pdf *= (1.0-FS)
	
	pdf2 = FS*sinTheta2L
	pdf2 += SS1*sinTheta2L*cosThetaK
	pdf2 += SS3*sinThetaL*sinThetaK*cosPhi
	pdf2 *= (3.0/(16.0*math.pi))
	
	pdf += pdf2
	
	return pdf

def FullAngDist_P6pfold_P_Swave(x, params_list):
	FL, P1, P6p, FS, SS1, SS4 =params_list
	
	cosThetaK = phsp.CosTheta1(x)
	cosThetaL = phsp.CosTheta2(x)
	phi = phsp.Phi(x)
	
	sinThetaK = tfa.Sqrt( 1.0 - cosThetaK * cosThetaK )
	sinThetaL = tfa.Sqrt( 1.0 - cosThetaL * cosThetaL )
	
	sinPhi=tfa.Sin(phi)
	cosPhi=tfa.Cos(phi)
	sin2Phi=tfa.Sin(2.0 * phi)
	cos2Phi=tfa.Cos(2.0 * phi)
	
	#sin^2(thetaK)
	sinTheta2K =  (1.0 - cosThetaK * cosThetaK)
	sinTheta2L =  (1.0 - cosThetaL * cosThetaL)
	cosTheta2L = cosThetaL*cosThetaL
	cosTheta2K = cosThetaK*cosThetaK
	
	#sin(2*thetaK)
	sin2ThetaK = (2.0 * sinThetaK * cosThetaK)
	sin2ThetaL = (2.0 * sinThetaL * cosThetaL)
	cos2ThetaL = (2.0 * cosThetaL * cosThetaL - 1.0)
	cos2ThetaK = (2.0 * cosThetaK * cosThetaK - 1.0)
	
	pdf = (3.0/4.0)*(1.0-FL)*sinTheta2K
	pdf += FL*cosTheta2K
	pdf += (1.0/4.0)*(1.0-FL)*sinTheta2K*cos2ThetaL
	pdf += (-1.0)*FL*cosTheta2K*cos2ThetaL
	pdf += (1.0/2.0)*(1.0-FL)*P1*sinTheta2K*sinTheta2L*cos2Phi 
	pdf += (tfa.Sqrt((1-FL)*FL))*P6p*sin2ThetaK*sinThetaL*sinPhi
	pdf *= (9.0/(32.0*math.pi))
	pdf *= (1.0-FS)
	
	pdf2 = FS*sinTheta2L
	pdf2 += SS1*sinTheta2L*cosThetaK
	pdf2 += SS4*sinThetaL*sinThetaK*sinPhi
	pdf2 *= (3.0/(16.0*math.pi))
	
	pdf += pdf2
	
	return pdf
	
def FullAngDist_P8pfold_P_Swave(x, params_list):
	FL, P1, P8p, FS, SS4=params_list
	
	cosThetaK = phsp.CosTheta1(x)
	cosThetaL = phsp.CosTheta2(x)
	phi = phsp.Phi(x)
	
	sinThetaK = tfa.Sqrt( 1.0 - cosThetaK * cosThetaK )
	sinThetaL = tfa.Sqrt( 1.0 - cosThetaL * cosThetaL )
	
	sinPhi=tfa.Sin(phi)
	cosPhi=tfa.Cos(phi)
	sin2Phi=tfa.Sin(2.0 * phi)
	cos2Phi=tfa.Cos(2.0 * phi)
	
	#sin^2(thetaK)
	sinTheta2K =  (1.0 - cosThetaK * cosThetaK)
	sinTheta2L =  (1.0 - cosThetaL * cosThetaL)
	cosTheta2L = cosThetaL*cosThetaL
	cosTheta2K = cosThetaK*cosThetaK
	
	#sin(2*thetaK)
	sin2ThetaK = (2.0 * sinThetaK * cosThetaK)
	sin2ThetaL = (2.0 * sinThetaL * cosThetaL)
	cos2ThetaL = (2.0 * cosThetaL * cosThetaL - 1.0)
	cos2ThetaK = (2.0 * cosThetaK * cosThetaK - 1.0)
	
	pdf = (3.0/4.0)*(1.0-FL)*sinTheta2K
	pdf += FL*cosTheta2K
	pdf += (1.0/4.0)*(1.0-FL)*sinTheta2K*cos2ThetaL
	pdf += (-1.0)*FL*cosTheta2K*cos2ThetaL
	pdf += (1.0/2.0)*(1.0-FL)*P1*sinTheta2K*sinTheta2L*cos2Phi 
	pdf += (tfa.Sqrt((1-FL)*FL))*P8p*sin2ThetaK*sin2ThetaL*sinPhi
	pdf *= (9.0/(32.0*math.pi))
	pdf *= (1.0-FS)
	
	pdf2 = FS*sinTheta2L
	pdf2 += SS4*sinThetaL*sinThetaK*sinPhi
	pdf2 *= (3.0/(16.0*math.pi))
	
	pdf += pdf2
	
	return pdf
	
def FullAngDist_Othersfold_P_Swave(x, params_list):
	FL, P1, P2, P3, FS, SS1=params_list
	
	cosThetaK = phsp.CosTheta1(x)
	cosThetaL = phsp.CosTheta2(x)
	phi = phsp.Phi(x)
	
	sinThetaK = tfa.Sqrt( 1.0 - cosThetaK * cosThetaK )
	sinThetaL = tfa.Sqrt( 1.0 - cosThetaL * cosThetaL )
	
	sinPhi=tfa.Sin(phi)
	cosPhi=tfa.Cos(phi)
	sin2Phi=tfa.Sin(2.0 * phi)
	cos2Phi=tfa.Cos(2.0 * phi)
	
	#sin^2(thetaK)
	sinTheta2K =  (1.0 - cosThetaK * cosThetaK)
	sinTheta2L =  (1.0 - cosThetaL * cosThetaL)
	cosTheta2L = cosThetaL*cosThetaL
	cosTheta2K = cosThetaK*cosThetaK
	
	#sin(2*thetaK)
	sin2ThetaK = (2.0 * sinThetaK * cosThetaK)
	sin2ThetaL = (2.0 * sinThetaL * cosThetaL)
	cos2ThetaL = (2.0 * cosThetaL * cosThetaL - 1.0)
	cos2ThetaK = (2.0 * cosThetaK * cosThetaK - 1.0)
	
	pdf = (3.0/4.0)*(1.0-FL)*sinTheta2K
	pdf += FL*cosTheta2K
	pdf += (1.0/4.0)*(1.0-FL)*sinTheta2K*cos2ThetaL
	pdf += (-1.0)*FL*cosTheta2K*cos2ThetaL
	pdf += (1.0/2.0)*(1.0-FL)*P1*sinTheta2K*sinTheta2L*cos2Phi 
	pdf += 2.0*(1.0-FL)*P2*sinTheta2K*cosThetaL 
	pdf += -(1.0-FL)*P3*sinTheta2K*sinTheta2L*sin2Phi 
	pdf *= (9.0/(32.0*math.pi))
	pdf *= (1.0-FS)
	
	pdf2 = FS*sinTheta2L
	pdf2 += SS1*sinTheta2L*cosThetaK
	pdf2 *= (3.0/(16.0*math.pi))
	
	pdf += pdf2
	
	return pdf
	
# Code backup (old code) -----------------------------------------------------------------------------------------------------------------------------------------------------
#~ # Full angular distribution PDF in S-basis (Ref: CERN-PH-EP-2015-314, p. 3)
def FullAngDist_S(x, params_list):
	FL, S3, S4, S5, AFB, S7, S8, S9=params_list
	
	cosThetaK = phsp.CosTheta1(x)
	cosThetaL = phsp.CosTheta2(x)
	phi = phsp.Phi(x)
	
	sinThetaK = tfa.Sqrt( 1.0 - cosThetaK * cosThetaK )
	sinThetaL = tfa.Sqrt( 1.0 - cosThetaL * cosThetaL )
	
	sinPhi=tfa.Sin(phi)
	cosPhi=tfa.Cos(phi)
	sin2Phi=tfa.Sin(2.0 * phi)
	cos2Phi=tfa.Cos(2.0 * phi)
	
	#sin^2(thetaK)
	sinTheta2K =  (1.0 - cosThetaK * cosThetaK)
	sinTheta2L =  (1.0 - cosThetaL * cosThetaL)
	cosTheta2L = cosThetaL*cosThetaL
	cosTheta2K = cosThetaK*cosThetaK
	
	#sin(2*thetaK)
	sin2ThetaK = (2.0 * sinThetaK * cosThetaK)
	sin2ThetaL = (2.0 * sinThetaL * cosThetaL)
	cos2ThetaL = (2.0 * cosThetaL * cosThetaL - 1.0)
	cos2ThetaK = (2.0 * cosThetaK * cosThetaK - 1.0)

	pdf = (3.0/4.0)*(1.0-FL)*sinTheta2K
	pdf += FL*cosTheta2K
	pdf += (1.0/4.0)*(1.0-FL)*sinTheta2K*cos2ThetaL
	pdf += (-1.0) *FL*cosTheta2K*cos2ThetaL
	pdf += S3*sinTheta2K*sinTheta2L*cos2Phi
	pdf += S4*sin2ThetaK*sin2ThetaL*cosPhi
	pdf += S5*sin2ThetaK*sinThetaL*cosPhi
	pdf += (4.0/3.0)*AFB*sinTheta2K*cosThetaL
	pdf += S7*sin2ThetaK*sinThetaL*sinPhi
	pdf += S8*sin2ThetaK*sin2ThetaL*sinPhi
	pdf += S9*sinTheta2K*sinTheta2L*sin2Phi
	
	return pdf

#~ # Full angular distribution PDF in P-basis (Ref: CERN-PH-EP-2015-314, p. 3)
#~ def full_angular_dist_P(x, params_list):
	#~ FL, P1, P2, P3, P4p, P5p, P6p, P8p=params_list
	
	#~ cosThetaK = phsp.CosTheta1(x)
	#~ cosThetaL = phsp.CosTheta2(x)
	#~ phi = phsp.Phi(x)
	
	#~ sinThetaK = tfa.Sqrt( 1.0 - cosThetaK * cosThetaK )
	#~ sinThetaL = tfa.Sqrt( 1.0 - cosThetaL * cosThetaL )
	
	#~ sinPhi=tfa.Sin(phi)
	#~ cosPhi=tfa.Cos(phi)
	#~ sin2Phi=tfa.Sin(2.0 * phi)
	#~ cos2Phi=tfa.Cos(2.0 * phi)
	
	#~ #sin^2(thetaK)
	#~ sinTheta2K =  (1.0 - cosThetaK * cosThetaK)
	#~ sinTheta2L =  (1.0 - cosThetaL * cosThetaL)
	#~ cosTheta2L = cosThetaL*cosThetaL
	#~ cosTheta2K = cosThetaK*cosThetaK
	
	#~ #sin(2*thetaK)
	#~ sin2ThetaK = (2.0 * sinThetaK * cosThetaK)
	#~ sin2ThetaL = (2.0 * sinThetaL * cosThetaL)
	#~ cos2ThetaL = (2.0 * cosThetaL * cosThetaL - 1.0)
	#~ cos2ThetaK = (2.0 * cosThetaK * cosThetaK - 1.0)
	
	#~ pdf = (3.0/4.0)*(1.0-FL)*sinTheta2K
	#~ pdf += FL*cosTheta2K
	#~ pdf += (1.0/4.0)*(1.0-FL)*sinTheta2K*cos2ThetaL
	#~ pdf += (-1.0) *FL*cosTheta2K*cos2ThetaL
	#~ pdf += (1.0/2.0)*(1.0-FL)*P1*sinTheta2K*sinTheta2L*cos2Phi 
	#~ pdf += (tfa.Sqrt((1-FL)*FL))*P4p*sin2ThetaK*sin2ThetaL*cosPhi
	#~ pdf += (tfa.Sqrt((1-FL)*FL))*P5p*sin2ThetaK*sinThetaL*cosPhi
	#~ pdf += 2.0*(1.0-FL)*P2*sinTheta2K*cosThetaL 
	#~ pdf += (tfa.Sqrt((1-FL)*FL))*P6p*sin2ThetaK*sinThetaL*sinPhi
	#~ pdf += (tfa.Sqrt((1-FL)*FL))*P8p*sin2ThetaK*sin2ThetaL*sinPhi
	#~ pdf += -(1.0-FL)*P3*sinTheta2K*sinTheta2L*sin2Phi 
	#~ pdf = (9.0/(32.0*math.pi))*pdf	
	
	#~ return pdf
	
#~ def model_fold_S5(x) : 
	#~ cosThetaK = phsp.CosTheta1(x)
	#~ cosThetaL = phsp.CosTheta2(x)
	#~ phi = phsp.Phi(x)

	#~ sinThetaK = tfa.Sqrt( 1.0 - cosThetaK * cosThetaK )
	#~ sinThetaL = tfa.Sqrt( 1.0 - cosThetaL * cosThetaL )

	#~ sinTheta2K =  (1.0 - cosThetaK * cosThetaK)
	#~ sinTheta2L =  (1.0 - cosThetaL * cosThetaL)

	#~ sin2ThetaK = (2.0 * sinThetaK * cosThetaK)
	#~ cos2ThetaL = (2.0 * cosThetaL * cosThetaL - 1.0)

	#~ pdf  = (3.0/4.0) * (1.0 - FL ) * sinTheta2K
	#~ pdf +=  FL * cosThetaK * cosThetaK
	#~ pdf +=  (1.0/4.0) * (1.0 - FL) * sinTheta2K *  cos2ThetaL
	#~ pdf +=  (-1.0) * FL * cosThetaK * cosThetaK *  cos2ThetaL
	#~ pdf +=  S3 * sinTheta2K * sinTheta2L * tfa.Cos(2.0 * phi )
	#~ pdf +=  S5 * sin2ThetaK * sinThetaL * tfa.Cos( phi )
	
	#~ return pdf


