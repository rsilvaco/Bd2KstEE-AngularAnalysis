# File containing all functios used for folding data
import math

# Ref: CERN-THESIS-2013-145
# [cosThetaK, cosThetaL, phi]
def Folding(param_to_fold, ensemble_not_folded):
	folded_data_ensemble_list=[]
	for ensemble in ensemble_not_folded:
		ensemble_folded=np.copy(ensemble)
		for i in range(0, len(ensemble)):
			if param_to_fold=="S4" or param_to_fold=="P4p": # P4p folding below
				if ensemble[i][2]<0.0:
					ensemble_folded[i][2]=-(ensemble[i][2])
				if math.acos(ensemble[i][1])>(math.pi/2.0):
					ensemble_folded[i][2]=math.pi-ensemble_folded[i][2]
					ensemble_folded[i][1]=math.cos(math.pi-math.acos(ensemble[i][1]))
			if param_to_fold=="S5" or param_to_fold=="P5p": # P5p folding below
				if ensemble[i][2]<0.0:
					ensemble_folded[i][2]=-(ensemble[i][2])
				if math.acos(ensemble[i][1])>(math.pi/2.0):
					ensemble_folded[i][1]=math.cos(math.pi-math.acos(ensemble[i][1]))
			if param_to_fold=="S7" or param_to_fold=="P6p": # P6p folding below
				if ensemble[i][2]>(math.pi/2.0):
					ensemble_folded[i][2]=(math.pi-ensemble[i][2])
				if ensemble[i][2]<(-math.pi/2.0):
					ensemble_folded[i][2]=(-math.pi-ensemble[i][2])
				if math.acos(ensemble[i][1])>(math.pi/2.0):
					ensemble_folded[i][1]=math.cos(math.pi-math.acos(ensemble[i][1]))
			if param_to_fold=="S8" or param_to_fold=="P8p": # P8p folding below
				if ensemble[i][2]>(math.pi/2.0):
					ensemble_folded[i][2]=(math.pi-ensemble[i][2])
				if ensemble[i][2]<(-math.pi/2.0):
					ensemble_folded[i][2]=(-math.pi-ensemble[i][2])
				if math.acos(ensemble[i][1])>(math.pi/2.0):
					ensemble_folded[i][0]=math.cos(math.pi-math.acos(ensemble[i][0]))
					ensemble_folded[i][1]=math.cos(math.pi-math.acos(ensemble[i][1]))
			if param_to_fold=="Others": # FL, P1, P2, P3 folding below
				if ensemble[i][2]<0.0:
					ensemble_folded[i][2]=(ensemble[i][2]+math.pi)
		folded_data_ensemble_list.append(ensemble_folded)
	return folded_data_ensemble_list
	
	
# Code backup ---------------------------------------------------------------------------------------------------------------------
#~ def Folding_S4_P4p(ensemble_not_folded):
	#~ folded_data_ensemble_list=[]
	#~ for ensemble in ensemble_not_folded:
		#~ ensemble_folded=np.copy(ensemble)
		#~ for i in range(0, len(ensemble)):
			#~ if ensemble[i][2]<0.0:
				#~ ensemble_folded[i][2]=-(ensemble[i][2])
			#~ if math.acos(ensemble[i][1])>(math.pi/2.0):
				#~ ensemble_folded[i][2]=math.pi-ensemble_folded[i][2]
				#~ ensemble_folded[i][1]=math.cos(math.pi-math.acos(ensemble[i][1]))
		#~ folded_data_ensemble_list.append(ensemble_folded)
	#~ return folded_data_ensemble_list

#~ def Folding_S5_P5p(ensemble_not_folded):
	#~ folded_data_ensemble_list=[]
	#~ for ensemble in ensemble_not_folded:
		#~ ensemble_folded=np.copy(ensemble)
		#~ for i in range(0, len(ensemble)):
			#~ if ensemble[i][2]<0.0:
				#~ ensemble_folded[i][2]=-(ensemble[i][2])
			#~ if math.acos(ensemble[i][1])>(math.pi/2.0):
				#~ ensemble_folded[i][1]=math.cos(math.pi-math.acos(ensemble[i][1]))
		#~ folded_data_ensemble_list.append(ensemble_folded)
	#~ return folded_data_ensemble_list

#~ def Folding_S7_P6p(ensemble_not_folded):
	#~ folded_data_ensemble_list=[]
	#~ for ensemble in ensemble_not_folded:
		#~ ensemble_folded=np.copy(ensemble)
		#~ for i in range(0, len(ensemble)):
			#~ if ensemble[i][2]>(math.pi/2.0):
				#~ ensemble_folded[i][2]=(math.pi-ensemble[i][2])
			#~ if ensemble[i][2]<(-math.pi/2.0):
				#~ ensemble_folded[i][2]=(-math.pi-ensemble[i][2])
			#~ if math.acos(ensemble[i][1])>(math.pi/2.0):
				#~ ensemble_folded[i][1]=math.cos(math.pi-math.acos(ensemble[i][1]))
		#~ folded_data_ensemble_list.append(ensemble_folded)
	#~ return folded_data_ensemble_list
	
#~ def Folding_S8_P8p(ensemble_not_folded):
	#~ folded_data_ensemble_list=[]
	#~ for ensemble in ensemble_not_folded:
		#~ ensemble_folded=np.copy(ensemble)
		#~ for i in range(0, len(ensemble)):
			#~ if ensemble[i][2]>(math.pi/2.0):
				#~ ensemble_folded[i][2]=(math.pi-ensemble[i][2])
			#~ if ensemble[i][2]<(-math.pi/2.0):
				#~ ensemble_folded[i][2]=(-math.pi-ensemble[i][2])
			#~ if math.acos(ensemble[i][1])>(math.pi/2.0):
				#~ ensemble_folded[i][1]=math.cos(math.pi-math.acos(ensemble[i][1]))
				#~ ensemble_folded[i][2]=math.cos(math.pi-math.acos(ensemble[i][2]))
		#~ folded_data_ensemble_list.append(ensemble_folded)
	#~ return folded_data_ensemble_list
	
#~ def Folding_Others(ensemble_not_folded):
	#~ folded_data_ensemble_list=[]
	#~ for ensemble in ensemble_not_folded:
		#~ ensemble_folded=np.copy(ensemble)
		#~ for i in range(0, len(ensemble)):
			#~ if ensemble[i][2]<0.0:
				#~ ensemble_folded[i][2]=(ensemble[i][2]+math.pi)
		#~ folded_data_ensemble_list.append(ensemble_folded)
	#~ return folded_data_ensemble_list