import tensorflow as tf

from timeit import default_timer as timer

import sys, math, os
sys.path.append("..")

from TensorFlowAnalysis import *
from tensorflow.python.client import timeline

from ROOT import TH1F, TH2F, TCanvas, gROOT, gStyle

if __name__ == "__main__" : 

  cache = False
  norm_grid = 500
  toy_sample = 100000
  gradient = True

  for i in sys.argv[1:] :
    if i == "--nogpu" : 
      os.environ["CUDA_VISIBLE_DEVICES"] = ""
    if i == "--cache" : 
      cache = True
    if i == "--1m" : 
      norm_grid = 1000
      toy_sample = 1000000
    if i == "--nograd" : 
      gradient = False

  # Masses of the initial and final state particles 
  md  = 1.869
  mpi = 0.139
  mk  = 0.497

  # Create Dalitz plot phase space 
  dlz = DalitzPhaseSpace(mpi, mk, mpi, md)

  # Blatt-Weisskopf radial constants
  dd = Const(5.)
  dr = Const(1.5)

  # Auxiliary function to create the resonance of mass "m", width "w", spin "spin" in the 
  # channel "ch", as a function of Dalitz plot variables vector "x"
  def Resonance(x, m, w, spin, ch) : 
    if ch == 0 : 
      return BreitWignerLineShape(dlz.M2ab(x), m,  w, mpi, mk, mpi, md, dr, dd, spin, spin, barrierFactor = False)*\
             ZemachTensor(dlz.M2ab(x), dlz.M2ac(x), dlz.M2bc(x), Const(md**2), Const(mpi**2), Const(mk**2), Const(mpi**2), spin, cache = cache)
    if ch == 1 : 
      return BreitWignerLineShape(dlz.M2bc(x), m,  w, mk, mpi, mpi, md, dr, dd, spin, spin, barrierFactor = False)*\
             ZemachTensor(dlz.M2bc(x), dlz.M2ab(x), dlz.M2ac(x), Const(md**2), Const(mk**2), Const(mpi**2), Const(mpi**2), spin, cache = cache)
    if ch == 2 : 
      return BreitWignerLineShape(dlz.M2ac(x), m,  w, mpi, mpi, mk, md, dr, dd, spin, spin, barrierFactor = False)*\
             ZemachTensor(dlz.M2ac(x), dlz.M2bc(x), dlz.M2ab(x), Const(md**2), Const(mpi**2), Const(mpi**2), Const(mk**2), spin, cache = cache)
    if ch == 3 : 
      return GounarisSakuraiLineShape(dlz.M2ac(x), m,  w, mpi)*\
             ZemachTensor(dlz.M2ac(x), dlz.M2bc(x), dlz.M2ab(x), Const(md**2), Const(mpi**2), Const(mpi**2), Const(mk**2), spin, cache = cache)
    if ch == 4 : 
      return FlatteLineShape(dlz.M2ac(x), m, 0.09, 0.02, mpi, mpi, mk, mk)*\
             ZemachTensor(dlz.M2ac(x), dlz.M2bc(x), dlz.M2ab(x), Const(md**2), Const(mpi**2), Const(mpi**2), Const(mk**2), spin, cache = cache)

    return None

  # Initial complex amplitudes for resonance components, (ampl, phase)
  a  = []
  a += [ (2.666864, 160.480162) ]

  a += [ (1.560805, 214.065284) ]
  a += [ (0.491463,  64.390750) ]
  a += [ (0.034337, 111.974402) ]
  a += [ (0.0385497, 207.278721) ]
  a += [ (0.203222, 212.128769) ]
  a += [ (1.436933, 342.852060) ]
  a += [ (1.561670, 109.586718) ]

  a += [ (1.638345, 133.218073) ]
  a += [ (0.651326, 119.929357) ]
  a += [ (2.209476, 358.855281) ]
  a += [ (0.890176, 314.789317) ]
  a += [ (0.877231,  82.271754) ]

  a += [ (0.149579, 325.356816) ]
  a += [ (0.423211, 252.523919) ]
  a += [ (0.364030,  87.118694) ]
  a += [ (0.228236, 275.203595) ]
  a += [ (2.081620, 130.047574) ]

  # Transform initial values of amplitudes into Cartesian fit parameters 
  c = []
  for n,ai in enumerate(a) : 
    c += [ Complex(FitParameter("a%dr" % n, ai[0]*math.cos(ai[1]*math.pi/180.), -10., 10., 0.01),
                   FitParameter("a%di" % n, ai[0]*math.sin(ai[1]*math.pi/180.), -10., 10., 0.01) ) ]

  # Signal model as a function of Dalitz plot vector "x"
  def model(x) :
    ampl  = Complex(Const(0.), Const(0.))

    ampl += c[0]

    ampl += Complex(Const(1.), Const(0.))*Resonance(x, Const(0.775500), Const(0.149400), 1, 3)
    ampl += c[1]*Resonance(x, Const(0.522477), Const(0.453106), 0, 2)
    ampl += c[2]*Resonance(x, Const(1.0),   Const(0.4550),   1, 2)
    ampl += c[3]*Resonance(x, Const(0.782650), Const(0.008490), 1, 2)
    ampl += c[4]*Resonance(x, Const(0.97700),  Const(0.1),      0, 4)
    ampl += c[5]*Resonance(x, Const(1.033172), Const(0.087984), 0, 2)
    ampl += c[6]*Resonance(x, Const(1.275400), Const(0.185200), 2, 2)
    ampl += c[7]*Resonance(x, Const(1.434000), Const(0.173000), 0, 2)

    ampl += c[8]*Resonance(x, Const(0.891660), Const(0.050800), 1, 0)
    ampl += c[9]*Resonance(x, Const(1.414000), Const(0.232000), 1, 0)
    ampl += c[10]*Resonance(x, Const(1.414000), Const(0.290000), 0, 0)
    ampl += c[11]*Resonance(x, Const(1.425600), Const(0.098500), 2, 0)
    ampl += c[12]*Resonance(x, Const(1.717000), Const(0.322000), 1, 0)

    ampl += c[13]*Resonance(x, Const(0.891660), Const(0.050800), 1, 1)
    ampl += c[14]*Resonance(x, Const(1.414000), Const(0.232000), 1, 1)
    ampl += c[15]*Resonance(x, Const(1.414000), Const(0.290000), 0, 1)
    ampl += c[16]*Resonance(x, Const(1.425600), Const(0.098500), 2, 1)
    ampl += c[17]*Resonance(x, Const(1.717000), Const(0.322000), 1, 1)

    return Density( ampl )

  # Background model as a function of Dalitz plot vector "x". 
  # In this case, it is a constant density (==1)
  def bck_model(x) :
    return Ones(dlz.M2ab(x))

  # Set random seed for toy MC. 
  SetSeed(2)

  # Get placeholders for data and normalisation graphs
  data_ph = dlz.data_placeholder
  norm_ph = dlz.norm_placeholder

  # Create signal model graphs for data and normalisation
  sig_data_model = model(data_ph)
  sig_norm_model = model(norm_ph)

  # Create background model graphs for data and normalisation
  bck_data_model = bck_model(data_ph)
  bck_norm_model = bck_model(norm_ph)

  # Create normalisation graphs
  sig_norm = Integral( sig_norm_model )
  bck_norm = Integral( bck_norm_model )

  # Combined signal + background model, 90% signal and 10% background 
  exp_model = sig_data_model/sig_norm*0.9 + bck_data_model/bck_norm*0.1

  # Initialise TF
  sess = tf.Session()
  init = tf.global_variables_initializer()
  sess.run(init)

  # Generate normalisation sample, rectangular 500x500 sample in the Dalitz plot phase space
  norm_sample = sess.run( dlz.RectangularGridSample(norm_grid, norm_grid) )

  # Calculate initial signal and background normalisations 
  init_sig_norm = sess.run( sig_norm, { norm_ph : norm_sample } )
  init_bck_norm = sess.run( bck_norm, { norm_ph : norm_sample } )

  # Create the graph for initial combined model. 
  # It is needed separately from "exp_model" because "exp_model"
  # depends on both data and normalisation placeholders, 
  # while EstimateMaximum and RunToyMC expect only a single placeholder
  # Thus the normalisation needs to be precalculated
  init_exp_model = sig_data_model/init_sig_norm*0.9 + bck_data_model/init_bck_norm*0.1

  # Calculate the maximum of the density
  majorant = EstimateMaximum(sess, init_exp_model, data_ph, norm_sample)*1.1
  print "Maximum = ", majorant
  data_sample = RunToyMC( sess, init_exp_model, data_ph, dlz, toy_sample, majorant, chunk = 2000000)

#  options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
#  run_metadata = tf.RunMetadata()
  options = None
  run_metadata = None

  # Create the graph for unbinned negative log likelihood. 
  # The density is normalised by construction, so no need to 
  # provide the graph for normalisation
  nll = UnbinnedNLL( exp_model, Const(1.) )
  # Minimise NLL
  start = timer()
  results = RunMinuit(sess, nll, { data_ph : data_sample, norm_ph : norm_sample}, useGradient = gradient, 
                      options = options, run_metadata = run_metadata )
  end = timer()
  print(end - start) 

  # Create the graphs for fitted signal and background normalisations,
  # and the fitted combined model, in the same way as for the initial model
  fit_sig_norm = sess.run( sig_norm, { norm_ph : norm_sample } )
  fit_bck_norm = sess.run( bck_norm, { norm_ph : norm_sample } )
  fit_exp_model = sig_data_model/fit_sig_norm*0.9 + bck_data_model/fit_bck_norm*0.1

  # Geterate toy MC sample corresponding to the fitted result
  fit_sample = RunToyMC(sess, fit_exp_model, data_ph, dlz, 1000000, majorant, chunk = 2000000)

  # Plot the initial and fitted distributions
  h1 = TH2F("h1", "", 200, dlz.minab-0.2, dlz.maxab+0.2, 200, dlz.minbc-0.2 , dlz.maxbc+0.2 ) 
  h2 = TH2F("h2", "", 200, dlz.minab-0.2, dlz.maxab+0.2, 200, dlz.minbc-0.2 , dlz.maxbc+0.2 ) 

  h3 = TH1F("h3", "", 100, dlz.minab-0.2, dlz.maxab+0.2 ) 
  h4 = TH1F("h4", "", 100, dlz.minab-0.2, dlz.maxab+0.2 ) 
  h5 = TH1F("h5", "", 100, dlz.minbc-0.2, dlz.maxbc+0.2 ) 
  h6 = TH1F("h6", "", 100, dlz.minbc-0.2, dlz.maxbc+0.2 ) 
  h7 = TH1F("h7", "", 100, dlz.minac-0.2, dlz.maxac+0.2 ) 
  h8 = TH1F("h8", "", 100, dlz.minac-0.2, dlz.maxac+0.2 ) 

  for d in data_sample : 
    h1.Fill(d[0], d[1])
    h3.Fill(d[0])
    h5.Fill(d[1])

  m2ac = sess.run(dlz.M2ac(data_ph), feed_dict = { data_ph : data_sample} )
  for d in m2ac : 
    h7.Fill(d)

  for f in fit_sample : 
    h2.Fill(f[0], f[1])
    h4.Fill(f[0])
    h6.Fill(f[1])

  m2ac = sess.run(dlz.M2ac(data_ph), feed_dict = { data_ph : fit_sample} )
  for f in m2ac : 
    h8.Fill(f)

  gROOT.ProcessLine(".x lhcbstyle2.C")

  c = TCanvas("c","", 900, 600)
  c.Divide(3, 2)
  gStyle.SetPalette(107)
  h4.SetLineColor(2)
  h6.SetLineColor(2)
  h8.SetLineColor(2)
  h1.GetXaxis().SetTitle("M^{2}(K_{S}#pi^{+}) [GeV^{2}]")
  h1.GetYaxis().SetTitle("M^{2}(K_{S}#pi^{-}) [GeV^{2}]")
  h2.GetXaxis().SetTitle("M^{2}(K_{S}#pi^{+}) [GeV^{2}]")
  h2.GetYaxis().SetTitle("M^{2}(K_{S}#pi^{-}) [GeV^{2}]")
  h3.GetXaxis().SetTitle("M^{2}(K_{S}#pi^{+}) [GeV^{2}]")
  h5.GetXaxis().SetTitle("M^{2}(K_{S}#pi^{-}) [GeV^{2}]")
  h7.GetXaxis().SetTitle("M^{2}(#pi^{+}#pi^{-}) [GeV^{2}]")

  c.cd(1); h1.Draw("zcol")
  c.cd(2); h2.Draw("zcol")
  c.cd(4); h3.Draw("e"); h4.Scale(h3.Integral()/h4.Integral()); h4.Draw("h same")
  c.cd(5); h5.Draw("e"); h6.Scale(h5.Integral()/h6.Integral()); h6.Draw("h same")
  c.cd(6); h7.Draw("e"); h8.Scale(h7.Integral()/h8.Integral()); h8.Draw("h same")
  c.Update()
  c.Print("D2KsPiPi.pdf")

  f = open("timing.txt","a")
  f.write(str(sys.argv) + " : %f sec, %d iterations\n" % (end-start, results["iterations"]))
  f.close()

#  fetched_timeline = timeline.Timeline(run_metadata.step_stats)
#  chrome_trace = fetched_timeline.generate_chrome_trace_format()
#  with open('timeline.json', 'w') as f:
#    f.write(chrome_trace)
