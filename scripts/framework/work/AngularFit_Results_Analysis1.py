# Document for results analysis (pull/parameter distributions)
import random
import array
import math
import pickle
import argparse
import datetime
import time
import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np

import sys, os
sys.path.append("../")
import TensorFlowAnalysis as tfa
from tensorflow.python.client import timeline

from ROOT import TFile

mu=tfa.FitParameter("mu", 0.0, -1.0, 1.0, 0.01)
sigma=tfa.FitParameter("sigma", 1.0, 0.0, 10.0, 0.01)

# Used for fitting
def NormalPDF(x):
	x=x[:,0]
	norm_factor=(1/tfa.Sqrt(2*math.pi*(sigma**2)))
	func=norm_factor*tfa.Exp(-(((x-mu)**2)/(2*(sigma**2))))
	return func

# Not used for fitting (used for plotting fit results) (Ref: http://mathworld.wolfram.com/NormalDistribution.html, eq.10)
def NormCDF(x, mu, sigma):
	func=(0.5)*(1.0+math.erf((x-mu)/(sigma*np.sqrt(2))))
	return func

# Only get results with status=3 (full and accurate covariant matrix)
def GetGoodFits(fit_results):
	good_results=[]
	for result in fit_results:
		if result["status"]==3:
			good_results.append(result)
	return good_results

# Only get results where all parameters have error(s)!=0
def RemoveZeroErr(fit_results, parameters, symmetric):
	nonzerolist=[]
	for result in fit_results:
		checklist=[]
		for i in range(0, len(parameters)):
			if symmetric=="True":
				if type(result[parameters[i]])==tuple:
					checklist.append(result[parameters[i]][1])
			if symmetric=="False":
				if type(result[parameters[i]])==tuple:
					checklist.append(result[parameters[i]][2])
					checklist.append(result[parameters[i]][3])
		if (np.all(checklist))!=0:
			nonzerolist.append(result)
	return nonzerolist

# Get list of parameters/errors from fitting
# paramfit=GetList("P5p", results_good_no0, "False")
def GetList(parameter, fit_results, want_error):
	value_list=[]
	for i in range(0, len(fit_results)):
		if want_error=="False":
			value_list.append(fit_results[i][parameter][0])
		if want_error=="True":
			value_list.append(fit_results[i][parameter][1])
	return value_list

# Ref: http://physics.rockefeller.edu/luc/technical_reports/cdf5776_pulls.pdf
def GetPull(parameter, fit_results, parameter_true, symmetric): 
	pull_list=np.zeros((len(fit_results),1))
	for i in range(0, len(fit_results)): # Loop over fit result of every ensemble
		parameter_fit=fit_results[i][parameter][0]
		if symmetric=="True": # If error symmetric
			parameter_fit_err=fit_results[i][parameter][1]
			pull=(parameter_fit-parameter_true)/parameter_fit_err 
		if symmetric=="False": # If error asymmetric
			parameter_fit_err_pos=fit_results[i][parameter][2] 
			parameter_fit_err_neg=fit_results[i][parameter][3]
			if parameter_fit<=parameter_true:
				pull=(parameter_true-parameter_fit)/parameter_fit_err_pos 
			else:
				pull=(parameter_fit-parameter_true)/parameter_fit_err_neg 
		pull_list[i][0]=pull
	return pull_list
	
data_ph = tf.placeholder(tf.float64, shape=(None, None), name="data_ph")

init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)

nll = -tf.reduce_sum(tf.log(NormalPDF(data_ph))) # PDF already normalized 

options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
run_metadata = tf.RunMetadata()

pickle_in=open("folded_all_results_100", "rb") # CHECK
results=pickle.load(pickle_in)

results_good=GetGoodFits(results)
results_good_no0=RemoveZeroErr(results_good, results[0].keys(), "True")

# PlotParamResults('S5', results_good_no0, 'S5 (S5 folding, ensemble size 100, 500/500 ensembles)', -0.023, 30)
def PlotParamResults(parameter, fit_results, data_to_plot_label, true_value, binnum):
	data_to_plot=[]
	for i in range(0, len(fit_results)):
		data_to_plot.append(fit_results[i][parameter][0])
		
	data_to_plot_array=np.asarray(data_to_plot)
	data_to_plot_array_shaped=data_to_plot_array.reshape((len(data_to_plot_array), 1))
	
	# Get good starting values for Minuit
	global mu
	global sigma
	mu.update(sess, np.mean(data_to_plot_array_shaped))
	sigma.update(sess, np.std(data_to_plot_array_shaped))
	
	gaussian_fit_result=tfa.RunMinuit(sess, nll, { data_ph : data_to_plot_array_shaped}, options = options, run_metadata = run_metadata )
	print gaussian_fit_result
	
	n, bins, patches = plt.hist(data_to_plot, bins=binnum, label=data_to_plot_label) 
	plt.show()
	
	binwidth=bins[1]-bins[0] 
	bincenters=np.zeros((len(bins)-1, 1))
	for i in range(0, len(bins)-1):
		bincenters[i][0]=(bins[i]+bins[i+1])/2
		
	mu=gaussian_fit_result["mu"][0] 
	sigma=gaussian_fit_result["sigma"][0] 
	expected_vals=np.zeros((len(bins)-1, 1))
	for i in range(0, len(bins)-1):
		prob=(NormCDF(bins[i+1], mu, sigma)-NormCDF(bins[i], mu, sigma)) 
		expected=prob*len(data_to_plot)
		expected_vals[i][0]=expected
		
	f=plt.figure()
	ax= f.add_subplot(111)
	ax.errorbar(bincenters, n, xerr=0, yerr=np.sqrt(n), fmt='o', color='k') # Make Poisson error bars
	#ax.plot(bincenters, Probs*binwidth*len(data_to_plot), label='Fitted PDF', color='b') # PDF simple scaling
	ax.plot(bincenters, expected_vals, label='Gaussian PDF', color='b') # Expected event numbers via CDF
	ax.text(0.03,0.90, 'mu (true): %.3f' %(true_value), horizontalalignment='left', verticalalignment='center',
	transform = ax.transAxes)
	ax.text(0.03,0.85, 'mu (fit): %.3f +/- %.3f' %(gaussian_fit_result['mu'][0], gaussian_fit_result['mu'][1]), horizontalalignment='left',
	verticalalignment='center',
	transform = ax.transAxes)
	ax.text(0.03,0.80, 'sigma (fit): %.3f +/- %.3f' %(gaussian_fit_result['sigma'][0], gaussian_fit_result['sigma'][1]), horizontalalignment='left',
	verticalalignment='center', transform = ax.transAxes)
	plt.title(data_to_plot_label)
	plt.xlabel("Parameter values (fit)")
	plt.ylabel("Parameters (fit)/ %.3f" %(binwidth))
	ax.grid()
	ax.legend(loc='upper left', prop={'size': 12}, framealpha=0.0)
	#plt.show()

# PlotPullResults('S5', results_good_no0, 'S5 pull (S5 folding, ensemble size 100, 500/500 ensembles)', -0.023, 30, "True")
def PlotPullResults(parameter, fit_results, data_to_plot_label, true_value, binnum, symmetric):	
	pull_data=GetPull(parameter, fit_results, true_value, symmetric)
	
	# Get good starting values for Minuit
	global mu
	global sigma
	mu.update(sess, np.mean(pull_data))
	sigma.update(sess, np.std(pull_data))
	
	gaussian_fit_result=tfa.RunMinuit(sess, nll, { data_ph : pull_data}, options = options, run_metadata = run_metadata )
	print gaussian_fit_result
	
	n, bins, patches = plt.hist(pull_data, bins=binnum, label=data_to_plot_label) 
	
	# Get values for bin centers and binwidth
	binwidth=bins[1]-bins[0] 
	bincenters=np.zeros((len(bins)-1, 1))
	for i in range(0, len(bins)-1):
		bincenters[i][0]=(bins[i]+bins[i+1])/2
	
	# Do more exact scaling with CDF (for this use NormCDF - mu and sigma manually inputted)
	mu=gaussian_fit_result["mu"][0] 
	sigma=gaussian_fit_result["sigma"][0] 
	expected_vals=np.zeros((len(bins)-1, 1))
	for i in range(0, len(bins)-1):
		prob=(NormCDF(bins[i+1], mu, sigma)-NormCDF(bins[i], mu, sigma)) 
		expected=prob*len(fit_results)
		expected_vals[i][0]=expected
		
	f=plt.figure()
	ax= f.add_subplot(111)
	ax.errorbar(bincenters, n, xerr=0, yerr=np.sqrt(n), fmt='o', color='k') # Make Poisson error bars
	ax.plot(bincenters, expected_vals, label='Gaussian PDF', color='b') # Expected event numbers via CDF
	ax.text(0.03,0.90, 'mu (fit): %.3f +/- %.3f' %(gaussian_fit_result['mu'][0], gaussian_fit_result['mu'][1]), horizontalalignment='left',
	verticalalignment='center', transform = ax.transAxes)
	ax.text(0.03,0.85, 'sigma (fit): %.3f +/- %.3f' %(gaussian_fit_result['sigma'][0], gaussian_fit_result['sigma'][1]), horizontalalignment='left',
	verticalalignment='center', transform = ax.transAxes)
	plt.title(data_to_plot_label)
	plt.xlabel("Pull values")
	plt.ylabel("Pulls/ %.3f" %(binwidth))
	ax.grid()
	ax.legend(loc='upper left', prop={'size': 12}, framealpha=0.0)
	#plt.show()






