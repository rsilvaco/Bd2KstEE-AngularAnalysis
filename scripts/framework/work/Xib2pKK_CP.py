import tensorflow as tf
import numpy as np

import sys
sys.path.append("../")

from ROOT import TH1F, TH2F, TCanvas, TFile, TGaxis

from TensorFlowAnalysis import *

TGaxis().SetMaxDigits(2)

def OrbitalMomentum(spin, parity) : 
  l1 = (spin-1)/2     # Lowest possible momentum
  p1 = 2*(l1 % 2)-1   # P=(-1)^(L1+1), e.g. P=-1 if L=0
  if p1 == parity : return l1
  return l1+1

def CouplingSign(spin, parity) : 
  jp =  1
  jd =  0
  pp =  1
  pd = -1
  s = 2*(((jp+jd-spin)/2+1) % 2)-1
  s *= (pp*pd*parity)
  return s

if __name__ == "__main__" : 

  SetSeed(1)

  xibMass     = 5794.5e-3  # GeV
  protonMass  = 938.27e-3  # GeV
  kaonMass    = 493.677e-3 # GeV

  phsp = Baryonic3BodyPhaseSpace(kaonMass, protonMass, kaonMass, xibMass, mabrange = (0., 3.), symmetric = True)

  db = Const(5.)
  dr = Const(1.5)

  couplings = [
    (
        (Const(1.), FitParameter("L1520_Re1_CPV", 0.0, -1., 1., 0.1)), 
        (Const(0.), Const(0.)), 
        (Const(0.), Const(0.)), 
        (Const(0.), Const(0.)), 
    ),
    (
        (FitParameter("L1690_Re1_CPC", 1.2, -10., 10., 0.1), FitParameter("L1690_Re1_CPV", 0., -1., 1., 0.1)), 
        (Const(0.), Const(0.)), 
#        (FitParameter("L1690_Re2_CPC", 0.6, -10., 10., 0.1), FitParameter("L1690_Re2_CPV", 0., -10., 10., 0.1)), 
        (Const(0.), Const(0.)), 
        (Const(0.), Const(0.)), 
    ),
  ]

  switches = Switches(len(couplings))

  def model(flavour, x) : 

    m2pk1 = phsp.M2ab(x)
    m2pk2 = phsp.M2bc(x)

    p4k1, p4p, p4k2 = phsp.FinalStateMomenta(m2pk1, m2pk2, 0., 0., 0.)
    dp_theta_r, dp_phi_r, dp_theta_k, dp_phi_k = HelicityAngles3Body(p4k1, p4p, p4k2)

    resonances = [
        ( BreitWignerLineShape(m2pk1, Const(1.5195), Const(0.0156), protonMass, kaonMass, kaonMass, xibMass, dr, db, OrbitalMomentum(3, -1), 1), 3, -1 ),
        ( BreitWignerLineShape(m2pk1, Const(1.690),  Const(0.060),  protonMass, kaonMass, kaonMass, xibMass, dr, db, OrbitalMomentum(3, -1), 1), 3, -1 ),
    ]

    density = Const(0.)
    for pol_xib in [-1, 1] : 
      for pol_p in [-1, 1] : 
        ampl = Complex(Const(0.), Const(0.))
        for r,c,s in zip(resonances, couplings, switches) : 
          lineshape = r[0]
          spin = r[1]
          parity = r[2]
          if pol_p == -1 : 
            sign = CouplingSign(spin, parity)
            coupling1 = Complex(c[0][0] + flavour*c[0][1], c[1][0] + flavour*c[1][1]) * sign
            coupling2 = Complex(c[2][0] + flavour*c[2][1], c[3][0] + flavour*c[3][1]) * sign
          else : 
            coupling1 = Complex(c[0][0] + flavour*c[0][1], c[1][0] + flavour*c[1][1])
            coupling2 = Complex(c[2][0] + flavour*c[2][1], c[3][0] + flavour*c[3][1])

          ampl += Complex(s,Const(0.))*coupling1*lineshape*HelicityAmplitude3Body(dp_theta_r, dp_phi_r, dp_theta_k,dp_phi_k,1,spin,pol_xib, 1, 0, pol_p, 0)
          ampl += Complex(s,Const(0.))*coupling2*lineshape*HelicityAmplitude3Body(dp_theta_r, dp_phi_r, dp_theta_k,dp_phi_k,1,spin,pol_xib,-1, 0, pol_p, 0)

        density += Density(ampl)
    return density

  data_ph1 = phsp.Placeholder("data1")
  data_ph2 = phsp.Placeholder("data2")
  norm_ph  = phsp.norm_placeholder

  sess = tf.Session()
  init = tf.global_variables_initializer()
  sess.run(init)

  xib_model    = model( 1, data_ph1)
  xibbar_model = model(-1, data_ph2)
  xib_norm_model    = model( 1, norm_ph)
  xibbar_norm_model = model(-1, norm_ph)

  norm_sample = sess.run( phsp.RectangularGridSample(400, 400) )

  maximum_toy_xib = EstimateMaximum(sess, xib_model, data_ph1, norm_sample)*1.5 

  data_toy_xib    = RunToyMC( sess, xib_model, data_ph1, phsp, 12000, maximum_toy_xib, chunk = 1000000)
  f_toy_xib    = TFile.Open("toy_xib.root", "RECREATE")
  FillNTuple("toy", data_toy_xib,    ["m2dp", "m2ppi" ] )
  f_toy_xib.Close()

  data_toy_xibbar = RunToyMC( sess, xib_model, data_ph1, phsp,  8000, maximum_toy_xib, chunk = 1000000) 
  f_toy_xibbar = TFile.Open("toy_xibbar.root", "RECREATE")
  FillNTuple("toy", data_toy_xibbar, ["m2dp", "m2ppi" ] )
  f_toy_xibbar.Close()

  norm = Integral(xib_norm_model) + Integral(xibbar_norm_model)
  nll = UnbinnedNLL(xib_model, norm) + UnbinnedNLL(xibbar_model, norm)

  result = RunMinuit(sess, nll, {data_ph1 : data_toy_xib, data_ph2 : data_toy_xibbar, norm_ph : norm_sample } )

  WriteFitResults(result, "results.txt")

  ff_xib    = CalculateFitFractions(sess, xib_norm_model,    norm_ph, switches, norm_sample )
  ff_xibbar = CalculateFitFractions(sess, xibbar_norm_model, norm_ph, switches, norm_sample )
  ff_cpc, ff_cpv    = CalculateCPFitFractions(sess, xib_norm_model, xibbar_norm_model, norm_ph, switches, norm_sample )

  WriteFitFractions(ff_xib,    ["L1520", "L1690"], "fitfractions_xib.txt")
  WriteFitFractions(ff_xibbar, ["L1520", "L1690"], "fitfractions_xibbar.txt")
  WriteFitFractions(ff_cpc,    ["L1520", "L1690"], "fitfractions_cpc.txt")
  WriteFitFractions(ff_cpv,    ["L1520", "L1690"], "fitfractions_cpv.txt")

  maximum_toy_xib       = EstimateMaximum(sess, xib_model,    data_ph1, norm_sample)*1.5 
  fit_data_xib    = RunToyMC(sess, xib_model,    data_ph1, phsp, 100000, maximum_toy_xib,    chunk = 1000000, switches = switches )
  maximum_toy_xibbar    = EstimateMaximum(sess, xibbar_model, data_ph2, norm_sample)*1.5 
  fit_data_xibbar = RunToyMC(sess, xibbar_model, data_ph2, phsp, 100000, maximum_toy_xibbar, chunk = 1000000, switches = switches )

  f = TFile.Open("fitresult_xib.root", "RECREATE")
  FillNTuple("toy", fit_data_xib, ["m2dp", "m2ppi" ] + [ "w%d" % (n+1) for n in range(len(switches)) ])
  f.Close()

  f = TFile.Open("fitresult_xibbar.root", "RECREATE")
  FillNTuple("toy", fit_data_xibbar, ["m2dp", "m2ppi" ] + [ "w%d" % (n+1) for n in range(len(switches)) ])
  f.Close()

  h2d_toy_xib = TH2F("h2d_toy_xib", "", 100, phsp.minab-0.2, phsp.maxab+0.2, 100, phsp.minbc-0.2 , phsp.maxbc+0.2 ) 
  hab_toy_xib = TH1F("hab_toy_xib", "", 100, phsp.minab-0.2, phsp.maxab+0.2 ) 
  hbc_toy_xib = TH1F("hbc_toy_xib", "", 100, phsp.minbc-0.2, phsp.maxbc+0.2 ) 

  h2d_toy_xibbar = TH2F("h2d_toy_xibbar", "", 100, phsp.minab-0.2, phsp.maxab+0.2, 100, phsp.minbc-0.2 , phsp.maxbc+0.2 ) 
  hab_toy_xibbar = TH1F("hab_toy_xibbar", "", 100, phsp.minab-0.2, phsp.maxab+0.2 ) 
  hbc_toy_xibbar = TH1F("hbc_toy_xibbar", "", 100, phsp.minbc-0.2, phsp.maxbc+0.2 ) 

  h2d_fit_xib = TH2F("h2d_fit_xib", "", 100, phsp.minab-0.2, phsp.maxab+0.2, 100, phsp.minbc-0.2 , phsp.maxbc+0.2 ) 
  hab_fit_xib = TH1F("hab_fit_xib", "", 100, phsp.minab-0.2, phsp.maxab+0.2 ) 
  hbc_fit_xib = TH1F("hbc_fit_xib", "", 100, phsp.minbc-0.2, phsp.maxbc+0.2 ) 

  h2d_fit_xibbar = TH2F("h2d_fit_xibbar", "", 100, phsp.minab-0.2, phsp.maxab+0.2, 100, phsp.minbc-0.2 , phsp.maxbc+0.2 ) 
  hab_fit_xibbar = TH1F("hab_fit_xibbar", "", 100, phsp.minab-0.2, phsp.maxab+0.2 ) 
  hbc_fit_xibbar = TH1F("hbc_fit_xibbar", "", 100, phsp.minbc-0.2, phsp.maxbc+0.2 ) 

  for d in data_toy_xib: 
    h2d_toy_xib.Fill(d[0], d[1])
    hab_toy_xib.Fill(d[0])
    hbc_toy_xib.Fill(d[1])

  for d in data_toy_xibbar: 
    h2d_toy_xibbar.Fill(d[0], d[1])
    hab_toy_xibbar.Fill(d[0])
    hbc_toy_xibbar.Fill(d[1])

  for d in fit_data_xib : 
    h2d_fit_xib.Fill(d[0], d[1])
    hab_fit_xib.Fill(d[0])
    hbc_fit_xib.Fill(d[1])

  for d in fit_data_xibbar: 
    h2d_fit_xibbar.Fill(d[0], d[1])
    hab_fit_xibbar.Fill(d[0])
    hbc_fit_xibbar.Fill(d[1])

  c = TCanvas("c","", 600, 600)
  c.Divide(2, 2)
  hab_toy_xib.SetLineColor(2)
  hbc_toy_xib.SetLineColor(2)
  hab_fit_xib.SetLineColor(3)
  hbc_fit_xib.SetLineColor(3)
  c.cd(1); h2d_toy_xib.Draw("zcol")
  c.cd(2); h2d_fit_xib.Draw("zcol")
  c.cd(3); 
  hab_toy_xib.Draw("e")
  hab_fit_xib.Scale(hab_toy_xib.Integral()/hab_fit_xib.Integral()); hab_fit_xib.Draw("h same")
  c.cd(4); 
  hbc_toy_xib.Draw("e")
  hbc_fit_xib.Scale(hbc_toy_xib.Integral()/hbc_fit_xib.Integral()); hbc_fit_xib.Draw("h same")
  c.SaveAs("xib_toyfit.pdf")

  c1 = TCanvas("c","", 600, 600)
  c1.Divide(2, 2)
  hab_toy_xibbar.SetLineColor(2)
  hbc_toy_xibbar.SetLineColor(2)
  hab_fit_xibbar.SetLineColor(3)
  hbc_fit_xibbar.SetLineColor(3)
  c1.cd(1); h2d_toy_xibbar.Draw("zcol")
  c1.cd(2); h2d_fit_xibbar.Draw("zcol")
  c1.cd(3); 
  hab_toy_xibbar.Draw("e")
  hab_fit_xibbar.Scale(hab_toy_xibbar.Integral()/hab_fit_xibbar.Integral()); hab_fit_xibbar.Draw("h same")
  c1.cd(4); 
  hbc_toy_xibbar.Draw("e")
  hbc_fit_xibbar.Scale(hbc_toy_xibbar.Integral()/hbc_fit_xibbar.Integral()); hbc_fit_xibbar.Draw("h same")
  c1.SaveAs("xibbar_toyfit.pdf")
