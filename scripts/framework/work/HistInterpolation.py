import tensorflow as tf

import sys, os
sys.path.append("../")
os.environ["CUDA_VISIBLE_DEVICES"] = ""

from TensorFlowAnalysis import *

from ROOT import TFile, TH2F, TCanvas

if __name__ == "__main__" : 

  mpi = 0.139
  mk  = 0.497
  md  = 1.865

  # Dalitz plot phase space for D->Kpipi
  phsp = DalitzPhaseSpace(mpi, mk, mpi, md)

  # Create the graph for uniform sample over the phase space
  uniform_sample = phsp.UniformSample(10000)

  # Initialise TF
  init = tf.global_variables_initializer()
  sess = tf.Session()
  sess.run(init)

  # Create the small uniform sample to fill the 2D histogram
  # to be used for bilinear interpolation
  uniform_data = sess.run(uniform_sample)

  # 2D histogram to fill
  h = TH2F("h","", 20, 0., 3., 20, 0., 3.)
  for i in uniform_data : 
    h.Fill(i[0], i[1])

  # Smoothen and plot the 2D histogram 
  c = TCanvas("c","c",600, 300)
  c.Divide(2, 1)
  c.cd(1)

  h.Smooth()
  h.Smooth()
  h.Draw("zcol")

  # Create the object for interpolated shape over the 2D histogram
  interp_shape = RootHistShape(h)

  # Numbers of bins for interpolated shape
  nbinsx = 200
  nbinsy = 200

  # Create the fine grid of points and calculate the interpolated shape
  grid = phsp.RectangularGridSample(nbinsx, nbinsy)
  model = interp_shape.shape(grid)

  grid_y = sess.run(model)
  grid_x = sess.run(grid)

  # Fill finer histogram and show the interpolated shape
  h2 = TH2F("h2","", nbinsx-1, phsp.minab, phsp.maxab, nbinsy-1, phsp.minbc, phsp.maxbc)
  for y,x in zip(grid_y, grid_x) : 
    h2.Fill(x[0], x[1], y)

  c.cd(2)
  h2.Draw("zcol")

  c.Update()
