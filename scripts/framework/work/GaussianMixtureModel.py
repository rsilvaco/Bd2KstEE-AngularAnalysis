import tensorflow as tf

from timeit import default_timer as timer

import sys, os
sys.path.append("../")
#os.environ["CUDA_VISIBLE_DEVICES"] = ""

from ROOT import TH1F, TH2F, TCanvas, TFile, gStyle, gROOT

from TensorFlowAnalysis import *

phsp = RectangularPhaseSpace( ((-1., 1.), (-1., 1.)) )

SetSeed(1)
gmm = GaussianMixture2D("gmm", 5, (-1, 1), (-1, 1) )

sess = tf.Session()
init = tf.global_variables_initializer()
sess.run(init)

norm_sample = sess.run( phsp.RectangularGridSample( (200, 200) ) )
print "Normalisation sample size = ", len(norm_sample)
print norm_sample

def true_pdf(z) : 
  x = phsp.Coordinate(z, 0)
  y = phsp.Coordinate(z, 1)
  r = Sqrt(x**2 + y**2)
  return Exp(-0.5*(r-0.5)**2/0.1**2)

data_ph = phsp.data_placeholder
norm_ph = phsp.norm_placeholder

true_model = true_pdf(data_ph)

data_sample = RunToyMC(sess, true_model, data_ph, phsp, 10000, 1. )

print data_sample

data_model = gmm.model(data_ph)
norm_model = gmm.model(norm_ph)

print gmm.params

print tf.trainable_variables()

nll = UnbinnedNLL( data_model, Integral( norm_model ) )
result = RunMinuit(sess, nll, { data_ph : data_sample, norm_ph : norm_sample }, printout = 100 )

print result

h1 = TH2F("h1","",100, -1., 1., 100, -1., 1.)
h2 = TH2F("h2","",100, -1., 1., 100, -1., 1.)

toy_sample = RunToyMC(sess, data_model, data_ph, phsp, 10000, 1. )

for i in data_sample : h1.Fill(i[0], i[1])
for i in toy_sample : h2.Fill(i[0], i[1])

c = TCanvas("c","c", 600, 300)
c.Divide(2,1)
c.cd(1)
h1.Draw("zcol")
c.cd(2)
h2.Draw("zcol")
