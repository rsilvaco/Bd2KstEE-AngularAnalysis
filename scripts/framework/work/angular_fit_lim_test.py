# This document contains PDF models for fitting
import sys, os, math, pickle
sys.path.append("../")

import TensorFlowAnalysis as tfa
import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np

phsp = tfa.FourBodyAngularPhaseSpace()

# Model
def FullAngDist_P5pfold(x, params_list): 
	FL, P1, P5p=params_list
	
	cosThetaK = phsp.CosTheta1(x)
	cosThetaL = phsp.CosTheta2(x)
	phi = phsp.Phi(x)
	
	sinThetaK = tfa.Sqrt( 1.0 - cosThetaK * cosThetaK )
	sinThetaL = tfa.Sqrt( 1.0 - cosThetaL * cosThetaL )
	
	sinPhi=tfa.Sin(phi)
	cosPhi=tfa.Cos(phi)
	sin2Phi=tfa.Sin(2.0 * phi)
	cos2Phi=tfa.Cos(2.0 * phi)
	
	#sin^2(thetaK)
	sinTheta2K =  (1.0 - cosThetaK * cosThetaK)
	sinTheta2L =  (1.0 - cosThetaL * cosThetaL)
	cosTheta2L = cosThetaL*cosThetaL
	cosTheta2K = cosThetaK*cosThetaK
	
	#sin(2*thetaK)
	sin2ThetaK = (2.0 * sinThetaK * cosThetaK)
	sin2ThetaL = (2.0 * sinThetaL * cosThetaL)
	cos2ThetaL = (2.0 * cosThetaL * cosThetaL - 1.0)
	cos2ThetaK = (2.0 * cosThetaK * cosThetaK - 1.0)
	
	pdf = (3.0/4.0)*(1.0-FL)*sinTheta2K
	pdf += FL*cosTheta2K
	pdf += (1.0/4.0)*(1.0-FL)*sinTheta2K*cos2ThetaL
	pdf += (-1.0)*FL*cosTheta2K*cos2ThetaL
	pdf += (1.0/2.0)*(1.0-FL)*P1*sinTheta2K*sinTheta2L*cos2Phi 
	pdf += (tfa.Sqrt((1-FL)*FL))*P5p*sin2ThetaK*sinThetaL*cosPhi
	pdf *= (9.0/(32.0*math.pi))
	
	return pdf

# Set model
model=FullAngDist_P5pfold # Get model to use from script containing models

# Model parameters
FL  = tfa.FitParameter("FL" ,  0.811620502,  -1.000,1.000, 0.001)
P1  = tfa.FitParameter("P1" ,  -0.115822186,  -10.000,10.000, 0.001)
P5p  = tfa.FitParameter("P5p" ,  -0.494968409,  -1.000,1.000, 0.001)

# Placeholders
data_ph = phsp.data_placeholder
norm_ph = phsp.norm_placeholder

# Tensorflow thread control
Num_Threads=1
init = tf.global_variables_initializer()
sess = tf.Session(config=tf.ConfigProto( intra_op_parallelism_threads=1,inter_op_parallelism_threads=Num_Threads)) 
sess.run(init)

# Norm sample (for numerical normalization)
norm_sample = sess.run( phsp.UniformSample(1000000) )

# Get toy number 10
use_file="myvals-P5p-ang-sig-only-150-test-gen_P5p_fold_gen_seed_11_poisson.pickle"
picin=open(use_file, "rb")
toys_dict=pickle.load(picin)
i_data_sample = toys_dict[10]["toy_df"][["cosThetaK", "cosThetaL", "phi", "q2", "B_M", "Kpi_M"]].values

# Set NLL
norm = tfa.Integral( model(norm_ph, [FL, P1, P5p]) )
nll = tfa.UnbinnedNLL( model(data_ph, [FL, P1, P5p]), norm )
	
# Fit
result=tfa.RunMinuit(sess, nll, { data_ph : i_data_sample, norm_ph : norm_sample }, call_limit=5000, runMinos=True, runHesse=True )
print(result)



	
