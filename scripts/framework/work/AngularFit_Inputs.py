# Script for input parameters (to models in AngularFit_Models.py)
import pickle

import sys, os
sys.path.append("../")
import TensorFlowAnalysis as tfa

# FL, P1, P2, P3, P4p, P5p, P6p, P8p, FS, SS1, SS2, SS3, SS4, SS5 - order is important!
# e.g. params_list=AFI.Params_FullAngDist_P_Swave(0.811620502, -0.115822186, -0.064241855, 0.004158823, -0.39227224, 
										#-0.494968409,-0.053616404,  -0.017899316, 0.0833, -0.2292, 0.0003, 0.0024, 0.0013, -0.0655)
def Params_FullAngDist_P_Swave(FL_val, P1_val, P2_val, P3_val, P4p_val, P5p_val, P6p_val, P8p_val, 
							FS_val, SS1_val, SS2_val, SS3_val, SS4_val, SS5_val, swave):
	params_list=[]
	FL  = tfa.FitParameter("FL" ,  FL_val,  -1.000,1.000, 0.001)
	P1  = tfa.FitParameter("P1" ,  P1_val,  -1.000,1.000, 0.001)
	P2  = tfa.FitParameter("P2" ,  P2_val,  -1.000,1.000, 0.001)
	P3  = tfa.FitParameter("P3" ,  P3_val,  -1.000,1.000, 0.001)
	P4p  = tfa.FitParameter("P4p" ,  P4p_val,  -1.000,1.000, 0.001)
	P5p  = tfa.FitParameter("P5p" ,  P5p_val,  -1.000,1.000, 0.001)
	P6p  = tfa.FitParameter("P6p" ,  P6p_val,  -1.000,1.000, 0.001)
	P8p  = tfa.FitParameter("P8p" ,  P8p_val,  -1.000,1.000, 0.001)
	if swave=="True":
		FS  = tfa.FitParameter("FS" ,  FS_val,  -1.000,1.000, 0.001) # S-wave fraction
		SS1  = tfa.FitParameter("SS1" ,  SS1_val,  -1.000,1.000, 0.001)
		SS2  = tfa.FitParameter("SS2" ,  SS2_val,  -1.000,1.000, 0.001)
		SS3  = tfa.FitParameter("SS3" ,  SS3_val,  -1.000,1.000, 0.001)
		SS4  = tfa.FitParameter("SS4" ,  SS4_val,  -1.000,1.000, 0.001)
		SS5  = tfa.FitParameter("SS5" ,  SS5_val,  -1.000,1.000, 0.001)
	if swave=="False":
		FS  = tfa.FitParameter("FS" ,  0,  -1.000,1.000, 0) # S-wave fraction
		SS1  = tfa.FitParameter("SS1" ,  0,  -1.000,1.000, 0)
		SS2  = tfa.FitParameter("SS2" ,  0,  -1.000,1.000, 0)
		SS3  = tfa.FitParameter("SS3" ,  0,  -1.000,1.000, 0)
		SS4  = tfa.FitParameter("SS4" ,  0,  -1.000,1.000, 0)
		SS5  = tfa.FitParameter("SS5" ,  0,  -1.000,1.000, 0)
	params_list.extend((FL, P1, P2, P3, P4p, P5p, P6p, P8p, FS, SS1, SS2, SS3, SS4, SS5))
	return params_list
	
# FL, P1, P4p, FS, SS1, SS2
def FullAngDist_P4pfold_P_Swave(FL_val, P1_val, P4p_val, FS_val, SS1_val, SS2_val, swave):
	params_list=[]
	FL  = tfa.FitParameter("FL" ,  FL_val,  -1.000,1.000, 0.001)
	P1  = tfa.FitParameter("P1" ,  P1_val,  -1.000,1.000, 0.001)
	P4p  = tfa.FitParameter("P4p" ,  P4p_val,  -1.000,1.000, 0.001)
	if swave=="True":
		FS  = tfa.FitParameter("FS" ,  FS_val,  -1.000,1.000, 0.001) # S-wave fraction
		SS1  = tfa.FitParameter("SS1" ,  SS1_val,  -1.000,1.000, 0.001)
		SS2  = tfa.FitParameter("SS2" ,  SS2_val,  -1.000,1.000, 0.001)
	if swave=="False":
		FS  = tfa.FitParameter("FS" ,  FS_val,  -1.000,1.000, 0) # S-wave fraction
		SS1  = tfa.FitParameter("SS1" ,  0,  -1.000,1.000, 0)
		SS2  = tfa.FitParameter("SS2" ,  0,  -1.000,1.000, 0)
	params_list.extend((FL, P1, P4p, FS, SS1, SS2))
	return params_list

# FL, P1, P5p, FS, SS1, SS3
def Params_FullAngDist_P5pfold_P_Swave(FL_val, P1_val, P5p_val, FS_val, SS1_val, SS3_val, swave):
	params_list=[]
	FL  = tfa.FitParameter("FL" ,  FL_val,  -1.000,1.000, 0.001)
	P1  = tfa.FitParameter("P1" ,  P1_val,  -1.000,1.000, 0.001)
	P5p  = tfa.FitParameter("P5p" ,  P5p_val,  -1.000,1.000, 0.001)
	if swave=="True": # Fitting to S-wave parameters
		FS  = tfa.FitParameter("FS" ,  FS_val,  -1.000,1.000, 0.001)
		SS1  = tfa.FitParameter("SS1" ,  SS1_val,  -1.000,1.000, 0.001)
		SS3  = tfa.FitParameter("SS3" ,  SS3_val,  -1.000,1.000, 0.001)
	if swave=="False":# NOT fitting to S-wave parameters
		FS  = tfa.FitParameter("FS" ,  0,  -1.000,1.000, 0) 
		SS1  = tfa.FitParameter("SS1" ,  0,  -1.000,1.000, 0)
		SS3  = tfa.FitParameter("SS3" ,  0,  -1.000,1.000, 0)
	params_list.extend((FL, P1, P5p, FS, SS1, SS3))
	return params_list

# FL, P1, P6p, FS, SS1, SS4
def Params_FullAngDist_P6pfold_P_Swave(FL_val, P1_val, P6p_val, FS_val, SS1_val, SS4_val, swave):
	params_list=[]
	FL  = tfa.FitParameter("FL" ,  FL_val,  -1.000,1.000, 0.001)
	P1  = tfa.FitParameter("P1" ,  P1_val,  -1.000,1.000, 0.001)
	P6p  = tfa.FitParameter("P6p" ,  P6p_val,  -1.000,1.000, 0.001)
	if swave=="True":
		FS  = tfa.FitParameter("FS" ,  FS_val,  -1.000,1.000, 0.001) # S-wave fraction
		SS1  = tfa.FitParameter("SS1" ,  SS1_val,  -1.000,1.000, 0.001)
		SS4  = tfa.FitParameter("SS4" ,  SS4_val,  -1.000,1.000, 0.001)
	if swave=="False":
		FS  = tfa.FitParameter("FS" ,  0,  -1.000,1.000, 0) # S-wave fraction
		SS1  = tfa.FitParameter("SS1" ,  0,  -1.000,1.000, 0)
		SS4  = tfa.FitParameter("SS4" ,  0,  -1.000,1.000, 0)
	params_list.extend((FL, P1, P6p, FS, SS1, SS4))
	return params_list

# FL, P1, P8p, FS, SS4
def Params_FullAngDist_P8pfold_P_Swave(FL_val, P1_val, P8p_val, FS_val, SS4_val, swave):
	params_list=[]
	FL  = tfa.FitParameter("FL" ,  FL_val,  -1.000,1.000, 0.001)
	P1  = tfa.FitParameter("P1" ,  P1_val,  -1.000,1.000, 0.001)
	P8p  = tfa.FitParameter("P8p" ,  P8p_val,  -1.000,1.000, 0.001)
	if swave=="True":
		FS  = tfa.FitParameter("FS" ,  FS_val,  -1.000,1.000, 0.001) # S-wave fraction
		SS4  = tfa.FitParameter("SS4" ,  SS4_val,  -1.000,1.000, 0.001)
	if swave=="False":
		FS  = tfa.FitParameter("FS" ,  0,  -1.000,1.000, 0) # S-wave fraction
		SS4  = tfa.FitParameter("SS4" ,  0,  -1.000,1.000, 0)
	params_list.extend((FL, P1, P8p, FS, SS4))
	return params_list

# FL, P1, P2, P3, FS, SS1
def Params_FullAngDist_Othersfold_P_Swave(FL_val, P1_val, P2_val, P3_val, FS_val, SS1_val, swave):
	params_list=[]
	FL  = tfa.FitParameter("FL" ,  FL_val,  -1.000,1.000, 0.001)
	P1  = tfa.FitParameter("P1" ,  P1_val,  -1.000,1.000, 0.001)
	P2  = tfa.FitParameter("P2" ,  P2_val,  -1.000,1.000, 0.001)
	P3  = tfa.FitParameter("P3" ,  P3_val,  -1.000,1.000, 0.001)
	if swave=="True":
		FS  = tfa.FitParameter("FS" ,  FS_val,  -1.000,1.000, 0.001) # S-wave fraction
		SS1  = tfa.FitParameter("SS1" ,  SS1_val,  -1.000,1.000, 0.001)
	if swave=="False":
		FS  = tfa.FitParameter("FS" ,  0,  -1.000,1.000, 0) # S-wave fraction
		SS1  = tfa.FitParameter("SS1" ,  0,  -1.000,1.000, 0)
	params_list.extend((FL, P1, P2, P3, FS, SS1))
	return params_list
	
# Data (from Flavio, S-wave from LHCb-ANA-2013-097, p.385) ---------------------------------------------------------------------------------------------------------------------------
# e.g. params_list=GetTheoryParamsList("2.5_4.0", "Params_FullAngDist_P_Swave", "True")
def GetTheoryParamsList(bin_name, model, swave):
	bin_name_short=bin_name.translate(None, '._')
	picsw=open("pred_dict_swave", "rb")
	dictsw=pickle.load(picsw)
	pic=open("pred_dict_"+bin_name_short, "rb")
	dict=pickle.load(pic) 
	
	# FL, P1, P2, P3, P4p, P5p, P6p, P8p, FS, SS1, SS2, SS3, SS4, SS5
	if model=="FullAngDist_P_Swave":
		return Params_FullAngDist_P_Swave(dict["FL_"+bin_name], dict["P1_"+bin_name], dict["P2_"+bin_name], dict["P3_"+bin_name], 
									dict["P4p_"+bin_name], dict["P5p_"+bin_name], dict["P6p_"+bin_name], dict["P8p_"+bin_name], 
									dictsw["FS"], dictsw["SS1"], dictsw["SS2"], dictsw["SS3"], dictsw["SS4"], dictsw["SS5"], swave)
	# FL, P1, P4p, FS, SS1, SS2
	if model=="FullAngDist_P4pfold_P_Swave":
		return Params_FullAngDist_P4pfold_P_Swave(dict["FL_"+bin_name], dict["P1_"+bin_name], dict["P4p_"+bin_name], 
											dictsw["FS"], dictsw["SS1"], dictsw["SS2"], swave)
	# FL, P1, P5p, FS, SS1, SS3
	if model=="FullAngDist_P5pfold_P_Swave":
		return Params_FullAngDist_P5pfold_P_Swave(dict["FL_"+bin_name], dict["P1_"+bin_name], dict["P5p_"+bin_name], 
											dictsw["FS"], dictsw["SS1"], dictsw["SS3"], swave)
	# FL, P1, P6p, FS, SS1, SS4
	if model=="FullAngDist_P6pfold_P_Swave":
		return Params_FullAngDist_P6pfold_P_Swave(dict["FL_"+bin_name], dict["P1_"+bin_name], dict["P6p_"+bin_name], 
											dictsw["FS"], dictsw["SS1"], dictsw["SS4"], swave)
	# FL, P1, P8p, FS, SS4
	if model=="FullAngDist_P8pfold_P_Swave":
		return Params_FullAngDist_P8pfold_P_Swave(dict["FL_"+bin_name], dict["P1_"+bin_name], dict["P8p_"+bin_name], 
											dictsw["FS"], dictsw["SS4"], swave)
	# FL, P1, P2, P3, FS, SS1
	if model=="FullAngDist_Othersfold_P_Swave":
		return Params_FullAngDist_Othersfold_P_Swave(dict["FL_"+bin_name], dict["P1_"+bin_name], dict["P2_"+bin_name],  
											dict["P3_"+bin_name], dictsw["FS"], dictsw["SS1"], swave)
	
	# FL, S3, S4, S5, AFB, S7, S8, S9 (old code)
	if model=="FullAngDist_S":
		return params_FullAngDist_S(dict["FL_"+bin_name], dict["S3_"+bin_name], dict["S4_"+bin_name], dict["S5_"+bin_name], 
											dict["AFB_"+bin_name], dict["S7_"+bin_name], dict["S8_"+bin_name], dict["S9_"+bin_name])
												
# Make dictionary of theoretical values (only need to run to get the files), values from Flavio, S-wave from LHCb-ANA-2013-097, p.385 -----------------------------------------
#~ names_1160=["dict", "FL_1.1_6.0", "S3_1.1_6.0", "S4_1.1_6.0", "S5_1.1_6.0", "AFB_1.1_6.0", "S7_1.1_6.0", "S8_1.1_6.0", "S9_1.1_6.0", 
		#~ "P1_1.1_6.0", "P2_1.1_6.0", "P3_1.1_6.0", "P4p_1.1_6.0", "P5p_1.1_6.0", "P6p_1.1_6.0", "P8p_1.1_6.0"]
#~ values_1160=["pred for 1.1<q^2<6.0GeV^2",0.76615677, -0.013110876, -0.148729086, -0.185480071, 0.007517143, -0.02017984,-0.006464733, -0.000788376,
			#~ -0.11221047849673184, 0.02144535471596485, 0.0033736906195486094,-0.3514980684261877, -0.4383533050306936,
			#~ -0.04769191393522267,-0.015278392134444789]
#~ names_1125=["dict", "FL_1.1_2.5", "S3_1.1_2.5", "S4_1.1_2.5", "S5_1.1_2.5", "AFB_1.1_2.5", "S7_1.1_2.5", "S8_1.1_2.5", "S9_1.1_2.5", 
		#~ "P1_1.1_2.5", "P2_1.1_2.5", "P3_1.1_2.5", "P4p_1.1_2.5", "P5p_1.1_2.5", "P6p_1.1_2.5", "P8p_1.1_2.5"]
#~ values_1125=["pred for 1.1<q^2<2.5GeV^2", 0.788966224, 0.002633534, -0.025278144, 0.057130988, -0.140801422, -0.028617718, -0.007478731,
			#~ -0.000857597, 0.024962381, -0.44486966, 0.004064437, -0.061954733, 0.140023533, -0.070139764, -0.018329777]
#~ names_2540=["dict", "FL_2.5_4.0", "S3_2.5_4.0", "S4_2.5_4.0", "S5_2.5_4.0", "AFB_2.5_4.0", "S7_2.5_4.0", "S8_2.5_4.0", "S9_2.5_4.0", 
		#~ "P1_2.5_4.0", "P2_2.5_4.0", "P3_2.5_4.0", "P4p_2.5_4.0", "P5p_2.5_4.0", "P6p_2.5_4.0", "P8p_2.5_4.0"]
#~ values_2540=["pred for 2.5<q^2<4.0GeV^2", 0.811620502, -0.010912711, -0.153408533, -0.193570612, -0.01815851, -0.020968126, -0.007000006,
			#~ -0.000783685, -0.115822186, -0.064241855, 0.004158823, -0.39227224, -0.494968409, -0.053616404, -0.017899316]
#~ names_4060=["dict", "FL_4.0_6.0", "S3_4.0_6.0", "S4_4.0_6.0", "S5_4.0_6.0", "AFB_4.0_6.0", "S7_4.0_6.0", "S8_4.0_6.0", "S9_4.0_6.0", 
		#~ "P1_4.0_6.0", "P2_4.0_6.0", "P3_4.0_6.0", "P4p_4.0_6.0", "P5p_4.0_6.0", "P6p_4.0_6.0", "P8p_4.0_6.0"]
#~ values_4060=["pred for 4.0<q^2<6.0GeV^2", 0.720770063, -0.024827923, -0.225847172, -0.337805231, 0.121354897, -0.014154202, 
			#~ -0.005373833, -0.000746465, -0.17781924, 0.289717286, 0.002673117, -0.503408411, -0.752960478, -0.031549407, -0.011978157]
#~ names_swave=["dict", "FS", "SS1", "SS2", "SS3", "SS4", "SS5"]
#~ values_swave=["Experimental S-wave (J/Psi)", 0.0833, -0.2292, 0.0003, 0.0024 , 0.0013, -0.0655]

#~ pred_dict_1160=dict(zip(names_1160, values_1160))
#~ picout_1160=open("pred_dict_1160", "wb")
#~ pickle.dump(pred_dict_1160, picout_1160)
#~ picout_1160.close()

#~ pred_dict_1125=dict(zip(names_1125, values_1125))
#~ picout_1125=open("pred_dict_1125", "wb")
#~ pickle.dump(pred_dict_1125, picout_1125)
#~ picout_1125.close()

#~ pred_dict_2540=dict(zip(names_2540, values_2540))
#~ picout_2540=open("pred_dict_2540", "wb")
#~ pickle.dump(pred_dict_2540, picout_2540)
#~ picout_2540.close()

#~ pred_dict_4060=dict(zip(names_4060, values_4060))
#~ picout_4060=open("pred_dict_4060", "wb")
#~ pickle.dump(pred_dict_4060, picout_4060)
#~ picout_4060.close()

#~ pred_dict_swave=dict(zip(names_swave, values_swave))
#~ picout_swave=open("pred_dict_swave", "wb")
#~ pickle.dump(pred_dict_swave, picout_swave)
#~ picout_swave.close()


# Code backup (old code) -----------------------------------------------------------------------------------------------------------------------------------------------------
# FL, S3, S4, S5, AFB, S7, S8, S9
def params_FullAngDist_S(FL_val, S3_val, S4_val, S5_val, AFB_val, S7_val, S8_val, S9_val):
	params_list=[]
	FL  = tfa.FitParameter("FL" ,  FL_val,  -1.000,1.000, 0.001)
	S3 = tfa.FitParameter("S3" ,  S3_val,  -1.000,1.000, 0.001)
	S4 = tfa.FitParameter("S4" ,  S4_val,  -1.000,1.000, 0.001)
	S5 = tfa.FitParameter("S5" ,  S5_val,  -1.000,1.000, 0.001)
	AFB = tfa.FitParameter("AFB" ,  AFB_val,  -1.000,1.000, 0.001)
	S7 = tfa.FitParameter("S7" ,  S7_val,  -1.000,1.000, 0.001)
	S8 = tfa.FitParameter("S8" ,  S8_val,  -1.000,1.000, 0.001)
	S9 = tfa.FitParameter("S9" ,  S9_val, -1.000,1.000, 0.001)
	params_list.extend((FL, S3, S4, S5, AFB, S7, S8, S9))
	return params_list

#~ #full_angular_dist_P(x, FL, P1, P2, P3, P4p, P5p, P6p, P8p)
#~ def params_full_angular_dist_P(FL_val, P1_val, P2_val, P3_val, P4p_val, P5p_val, P6p_val, P8p_val):
	#~ params_list=[]
	#~ FL  = tfa.FitParameter("FL" ,  FL_val,  -1.000,1.000, 0.001)
	#~ P1  = tfa.FitParameter("P1" ,  P1_val,  -1.000,1.000, 0.001)
	#~ P2  = tfa.FitParameter("P2" ,  P2_val,  -1.000,1.000, 0.001)
	#~ P3  = tfa.FitParameter("P3" ,  P3_val,  -1.000,1.000, 0.001)
	#~ P4p  = tfa.FitParameter("P4p" ,  P4p_val,  -1.000,1.000, 0.001)
	#~ P5p  = tfa.FitParameter("P5p" ,  P5p_val,  -1.000,1.000, 0.001)
	#~ P6p  = tfa.FitParameter("P6p" ,  P6p_val,  -1.000,1.000, 0.001)
	#~ P8p  = tfa.FitParameter("P8p" ,  P8p_val,  -1.000,1.000, 0.001)
	#~ params_list.extend((FL, P1, P2, P3, P4p, P5p, P6p, P8p))
	#~ return params_list
