# script for toy MC generation and fitting
# File for toy MC generation and folding
import random
import array
import math
import pickle
import argparse
import datetime
import time
import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np

import sys, os
sys.path.append("../")
import TensorFlowAnalysis as tfa
from tensorflow.python.client import timeline

from ROOT import TFile

import AngularFit_Inputs as AFI
import AngularFit_Models as AFM 
import AngularFit_Foldings as AFF

# python AngularFit_ToyMC_Fitting.py False False False ensemble_size max seed folding_type ensemble_start_num ensemble_end_num use_file results_file
parser = argparse.ArgumentParser()
parser.add_argument("generate_toy_MC", nargs='?', const=1, default="False")
parser.add_argument("fold_toy_MC", nargs='?', const=1, default="False")
parser.add_argument("fit", nargs='?', const=1, default="False")

parser.add_argument("max_ensembles", nargs='?', const=1, default=1, type=int)
parser.add_argument("ensemble_size", nargs='?', const=1, default=1000, type=int)
parser.add_argument("seed", nargs='?', const=1, default=1, type=int)
parser.add_argument("folding_type", nargs='?', const=1, default="S5")
parser.add_argument("ensemble_start_num", type=int, nargs='?', const=1, default=0)
parser.add_argument("ensemble_end_num", type=int, nargs='?', const=1, default=1)

parser.add_argument("use_file", nargs='?', const=1, default="somefile")
parser.add_argument("results_file", nargs='?', const=1, default="some_results"+str(datetime.datetime.now()))

args = parser.parse_args()

codestamp=np.random.randint(10000000) # For record keeping

phsp = tfa.FourBodyAngularPhaseSpace()

# Variables that should be CHECKED before EACH run! ----------------------------------------------------------------------------------------------------
params_list=AFI.GetTheoryParamsList("2.5_4.0", "FullAngDist_S", "False")
model=AFM.FullAngDist_S
#---------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Set common path for outputs
output_path=os.getcwd()+"/Results"
if not os.path.exists(output_path):
	os.makedirs(output_path)
	
data_ph = phsp.data_placeholder
norm_ph = phsp.norm_placeholder

Num_Threads=1 # CHECK

init = tf.global_variables_initializer()

sess = tf.Session(config=tf.ConfigProto( intra_op_parallelism_threads=1, inter_op_parallelism_threads=Num_Threads)) # CHECK
sess.run(init)

norm_sample = sess.run( phsp.UniformSample(1000000) )
majorant = tfa.EstimateMaximum(sess, model(data_ph, params_list), data_ph, norm_sample )*1.1

options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
run_metadata = tf.RunMetadata()

if args.generate_toy_MC=="True":
	ensemble_size=args.max_ensembles
	max=args.ensemble_size
	seed=args.seed
	all_toys_num=ensemble_size*max
	
	all_toys=tfa.RunToyMC( sess, model(data_ph, params_list), data_ph, phsp, all_toys_num, majorant, chunk = 10000, seed=seed)

	data_ensemble_array=all_toys.reshape(max, ensemble_size, 3)
	
	picout=open(output_path+"/"+str(codestamp)+"_"+args.results_file, "wb")
	pickle.dump(data_ensemble_array, picout)
	picout.close()

if args.fold_toy_MC=="True":
	
	picin=open(args.use_file, "rb")
	ensemble_not_folded=pickle.load(picin)

	ensemble_folded=AFF.Folding(args.folding_type, ensemble_not_folded)
	
	picout=open(output_path+"/"+str(codestamp)+"_"+args.results_file, "wb")
	pickle.dump(ensemble_folded, picout)
	picout.close()
	
if args.fit=="True":
	start=time.time()

	picin=open(args.use_file, "rb")
	ensemble_not_folded=pickle.load(picin)
	
	norm = tfa.Integral(model(norm_ph, params_list))
	nll = tfa.UnbinnedNLL(model(data_ph, params_list), norm)

	options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
	run_metadata = tf.RunMetadata()
	
	# e.g. do 0-100, 100-200, 200-300... will give full coverage 
	ensemble_start_num=args.ensemble_start_num
	ensemble_end_num=args.ensemble_end_num
	
	results=[]
	for i in range(ensemble_start_num, ensemble_end_num):
		result=tfa.RunMinuit(sess, nll, { data_ph : ensemble_not_folded[i], norm_ph : norm_sample }, call_limit=5000, runMinos=False, runHesse=True, options = options, run_metadata = run_metadata )
		results.append(result)
	
	end=time.time()
	
	pickle_out=open(output_path+"/"+str(codestamp)+"_"+args.results_file, "wb")
	pickle.dump(results, pickle_out)
	pickle_out.close()


# Record keeping ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
parameters=tf.trainable_variables() 
with open(output_path+"/"+str(codestamp)+"_RunLog.txt", "a") as file:
	file.write("AngularFit_ToyMC.py\n")
	file.write(str(datetime.datetime.now())+"\n")
	file.write("Run code: "+str(codestamp)+"\n")
	file.write("\n")
	file.write("Model used: "+str(model)+"\n")
	file.write("Folding used: "+str(args.folding_type)+"\n")
	file.write(str(args)+"\n")
	file.write("\n")
	file.write("FitParameters used:\n") #gen_val = { p.par_name : p.init_value for p in tfpars }!!!! consider e.g. tfpars = [datamu, datasigma1, datasigma2, MCa1, MCn1, MCa2, MCn2, datafCB1, slope, NSig, NBkg ]#dataf, p.par_name gets actual name of parameter!
	for parameter in parameters:
		file.write(str(parameter.par_name)+" "+str(parameter.init_value)+"\n")
	file.write("\n")
	
	if args.generate_toy_MC=="True":
		file.write("Generated toy MC\n")
		file.write("Using single seed for all ensembles\n")
		file.write("Ensemble size: "+str(ensemble_size)+"\n")
		file.write("Seed: "+str(seed)+"\n")
		file.write("Number of ensembles: "+str(max)+"\n")
		file.write("Saved results in: "+str(codestamp)+"_"+args.results_file+"\n")
		file.write("\n")
		
	if args.fold_toy_MC=="True":
		file.write("Folded toy MC\n")
		file.write("Used data from: "+args.use_file+"\n")
		file.write("Saved results in: "+str(codestamp)+"_"+args.results_file+"\n")
		file.write("\n")
	
	if args.fit=="True":
		file.write("Fitted PDF to data\n")
		file.write("Used data from: "+args.use_file+"\n")
		file.write("Fitting from ensemble: "+str(args.ensemble_start_num)+"\n")
		file.write("Fitting to ensemble: "+str(args.ensemble_end_num)+"\n")
		file.write("Minos=false, Hesse=true\n")
		file.write("Time elapsed: "+str(end-start)+"\n")
		file.write("Saved results in: "+str(codestamp)+"_"+args.results_file+"\n")
		file.write("\n")
		
fetched_timeline = timeline.Timeline(run_metadata.step_stats)
chrome_trace = fetched_timeline.generate_chrome_trace_format(show_memory=True)
with open(output_path+"/"+str(codestamp)+"_"+'timeline.json', 'w') as f:
	f.write(chrome_trace)

# Code backup (to use if required ensemble size is large) ---------------------------------------------------------------------------------------------------------------------
# Generate toy MC data using different seed for each ensemble
#~ if GenerateToyMCData=='True':
	
	#~ results_file="Bin2_PBasis_ToyMCData.txt"
	
	#~ data_ensemble_list=[]
	#~ ensemble_size=100 # Size of ensemble
	#~ count=0 
	#~ seed=6000
	#~ max=1000 # Max number of ensembles wanted
	
	#~ while count<max:
		#~ ensemble = tfa.RunToyMC( sess, model(data_ph, params_list), data_ph, phsp, ensemble_size, majorant, chunk = 10000, seed=seed)
		#~ count=count+1
		#~ seed=seed+1
		#~ data_ensemble_list.append(ensemble)
	#~ data_ensemble_array=np.asarray(data_ensemble_list)
	
	#~ # get back shape by doing e.g. x=np.loadtxt('data_ensemble_array_flattened'), array.reshape(500, 300, 3) 
	#~ np.savetxt(save_file, data_ensemble_array.flatten()) 

