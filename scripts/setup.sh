#!/bin/bash

shopt -s expand_aliases

## Define root variable
export ANAROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

set +u

## Setup with Snakemake, with cvmfs, with venv or with nothing
if [ "$1" == "snake" ]; then
    echo "Setup with snakemake"
    source activate snake
elif [ "$1" == "venv" ]; then 
    echo "Setup with venv"
    source $ANAROOT/scripts/setup_venv.sh
elif [ "$1" == "none" ]; then
    echo "Setup without snakemake, venv and cvmfs"
else
    echo "Setup with cvmfs"
    source $ANAROOT/scripts/setup_path.sh
    source $ANAROOT/scripts/setup_venv.sh
fi

## Setup the submodules
cd $ANAROOT/pyutils
source setup.sh
cd $ANAROOT

export PYTHONPATH=$ANAROOT:$ANAROOT/python:$PYTHONPATH

# Locations

export SCRIPTS=$ANAROOT/scripts
export PLOTS=$ANAROOT/output/plots
export TABLES=$ANAROOT/output/tables
export LOGS=$ANAROOT/output/logs

## Aliases

alias goAna='cd $ANAROOT'
alias runganga='ganga -i $ANAROOT/Ganga/gangaoption.py'

## Variables and aliases for snakemake

alias snakeclean='rm $(snakemake --summary | tail -n+2 | cut -f1)'
alias stripeff='lb-run LHCbDirac/prod dirac-bookkeeping-rejection-stats -BK '



